<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";
$required = 'required';
if($action == 'EDIT'){
  $required  = '';
  $sql   = "SELECT * FROM  t_blog WHERE tb_id = '$id'";
  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $tb_name      = $row[0]['tb_name'];
  $tb_detail    = $row[0]['tb_detail'];
  $tb_img       = $row[0]['tb_img'];
  $is_active    = $row[0]['is_active'];
}

?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="tb_id" value="<?=$id?>">
<div class="modal-body">
    <!-- /.box-header -->
      <!-- <div class="box-body"> -->
        <div class="row">
            <div class="col-md-9">
              <div class="form-group">
                <label>ชื่อบทความ</label>
                <input type="text" value="<?=@$tb_name?>" name="tb_name" class="form-control"  data-smk-msg="ชื่อบทความ" placeholder="ชื่อบทความ" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>สถานะ</label>
                <select name="is_active" class="form-control " style="width: 100%;" required >
                  <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
                  <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <input type="file" name="tb_img" onchange="readURL2(this,'showImage','width=100%')" <?=$required ?> style="font-size: 14px;">
              </div>
            </div>
            <div class="col-md-6" id="showImage">
              <?php if($action == 'EDIT'){ ?>
                <img width="100%" src="../../../web/assets/img/blog/<?=$tb_img?>">
              <?php } ?>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <div id="editor"><?=@$tb_detail?></div>
                <input type="hidden" id="detail" name="tb_detail">
              </div>
            </div>
        </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
    init();
    // ClassicEditor
    //   .create( document.querySelector( '#editor' ), {
    //       image: {
    //         styles: [
    //           'alignLeft',
    //           'alignCenter',
    //           'alignRight',
    //         ],
    //         resizeOptions: [
    //               {
    //                   name: 'imageResize:original',
    //                   label: 'Original',
    //                   value: null
    //               },
    //               {
    //                   name: 'imageResize:50',
    //                   label: '50%',
    //                   value: '50'
    //               },
    //               {
    //                   name: 'imageResize:75',
    //                   label: '75%',
    //                   value: '75'
    //               }
    //           ],
    //         toolbar: [
    //               'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
    //               '|',
    //               'imageTextAlternative'
    //           ]
    //       },
    //       table: {
    //           contentToolbar: [
    //               'tableColumn', 'tableRow', 'mergeTableCells'
    //           ]
    //       },
    //       mediaEmbed: {
    //           previewsInData: true,
    //       },
    //       ckfinder: {
    //         // eslint-disable-next-line max-len
    //         uploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
    //         options: {
    //             resourceType: 'Images'
    //         }
    //       }
    //   } )
    //   .then( editor => {
    //     dataEditor = editor;
    //   } )
    //   .catch( err => {
    //     console.error( err.stack );
    //   } );

  })
</script>

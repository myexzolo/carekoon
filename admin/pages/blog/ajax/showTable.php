<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
.wordWarp {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}
</style>
<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px">ลำดับ</th>
      <th style="width:20%">ชื่อบทความ</th>
      <th >ภาพหน้าบทความ</th>
      <th style="width:40%" >รายละเอียดบทความ</th>
      <th style="width:50px;">แก้ไข</th>
      <th style="width:50px;">ลบ</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT *
                 FROM  t_blog
                 where is_active not in ('D')
                 ORDER BY create_date DESC";
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];
      if($dataCount > 0){
        foreach ($rows as $key => $value) {
    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><div class="wordWarp"><?=strip_tags($value['tb_name'])?></div></td>
      <td><img width="100px" src="../../../web/assets/img/blog/<?=$value['tb_img']?>"></td>
      <td align="left" ><div class="wordWarp" style="width:400px;"><?=strip_tags($value['tb_detail'])?></div></td>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['tb_id']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['tb_id']?>')"></i></a>
      </td>
    </tr>
    <?php } ?>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>

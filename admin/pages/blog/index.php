<!DOCTYPE html>
  <?php
  $pageName     = "";
  $pageCode     = "";
  ?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <title>ระบบ Back Office - <?= $pageName ?></title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/blog.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php include("../../inc/sidebar.php"); ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              <?= $pageName ?>
              <small><?=$pageCode ?></small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="../../pages/home/"><i class="fa fa-home"></i> หน้าหลัก</a></li>
              <li class="active"><?= $pageName ?></li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php"); ?>
            <!-- Main row -->
            <div class="row">
              <div class="col-md-12">
                <div class="box box-info" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
                  <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    <div class="pull-right">
                      <div class="rows">
                        <div style="float: right;">
                          <!-- <button type="button" class="btn bg-navy btn-flat" onclick="exportCSV()" style="width:160px;font-size:20px;height:40px;">Export CSV file</button> -->
                          <button type="button" class="btn btn-success btn-flat" onclick="showForm('ADD','')" style="width:160px;font-size:20px;height:40px;">เพิ่มบทความ</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div id="showTable"></div>
                  </div>
                </div>
              </div>
            </div>

            <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Management Customer</h4>
                  </div>
                  <form id="formCustomer" novalidate enctype="multipart/form-data">
                  <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                    <div id="show-form"></div>
                  </form>
                </div>
              </div>
            </div>

            <div class="modal fade" id="myModalReset" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Management Reset Password</h4>
                  </div>
                  <form id="formResetPassword" novalidate enctype="multipart/form-data">
                  <!-- <form novalidate enctype="multipart/form-data" action="ajax/AED.php" method="post"> -->
                    <div id="show-form-rw"></div>
                  </form>
                </div>
              </div>
            </div>


            <!-- /.row -->
          </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script async charset="utf-8" src="//cdn.embedly.com/widgets/platform.js"></script>
      <script src="js/blog.js"></script>
    </body>
  </html>

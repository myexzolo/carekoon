$(function () {
  pushMenu();
  showTable();
})


function pushMenu()
{
  if($( window ).width() < 1900){
     $('[data-toggle="push-menu"]').pushMenu('toggle');
  }
}

function showTable(){
  $.get("ajax/showTable.php")
    .done(function( data ) {
      $('#showTable').html(data);
  });
}

function showForm(action,id){
  $.post("ajax/form.php",{action:action,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}


var init = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function() {
		var editorElement = CKEDITOR.document.getById( 'editor' );

		// :(((
		// if ( isBBCodeBuiltIn ) {
		// 	editorElement.setHtml(
		// 		'Hello world!\n\n' +
		// 		'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
		// 	);
		// }

		// Depending on the wysiwygarea plugin availability initialize classic or inline editor.
		if ( wysiwygareaAvailable ) {
      dataEditor = CKEDITOR.replace('editor', {
      extraPlugins: 'uploadimage,image2,autogrow',
      autoGrow_minHeight: 200,
      autoGrow_maxHeight: 600,
      autoGrow_bottomSpace: 50,
      removePlugins: 'resize',

      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      filebrowserBrowseUrl: '../../ckfinder/ckfinder.html',
      filebrowserImageBrowseUrl: '../../ckfinder/ckfinder.html?type=Images',
      // filebrowserUploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
      //filebrowserUploadUrl: 'base64',
      filebrowserImageUploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

      image2_alignClasses: [ 'image-align-left', 'image-align-center', 'image-align-right' ],
			image2_disableResizer: true
    });
		} else {
			editorElement.setAttribute( 'contenteditable', 'true' );
			CKEDITOR.inline( 'editor' );

			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
		}
	};

	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}
		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();

function del(id){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'DEL',tb_id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}


$('#formCustomer').on('submit', function(event) {
  event.preventDefault();
  if ($('#formCustomer').smkValidate()) {
    var detail = dataEditor.getData();
    $('#detail').val(detail);
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formCustomer').smkClear();
        showSlidebar();
        $('#myModal').modal('toggle')
        showTable();
        $.smkAlert({text: data.message,type: data.status});
      }, 1000);
    });
  }
});

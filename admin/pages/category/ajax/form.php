<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action       = @$_POST['value'];
$category_id  = @$_POST['category_id'];
$category_name_th  = "";
$category_name_en  = "";
$category_path  = "";

$required = "required";

if($action == "EDIT")
{
  $required = "";

  $sqls   = "SELECT * FROM t_category where category_id = '$category_id'";
  $querys = DbQuery($sqls,null);
  $json   = json_decode($querys, true);
  $counts = $json['dataCount'];
  $rows   = $json['data'];

  if($counts > 0){
    $category_name_th  = $rows[0]['category_name_th'];
    $category_name_en  = $rows[0]['category_name_en'];
    $category_path  = $rows[0]['category_path'];
  }
}

?>
<input type="hidden" id="action" name="action" value="<?= $action ?>">
<input type="hidden" name="category_id" value="<?=$category_id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>Category Name TH</label>
        <input value="<?=$category_name_th?>" name="category_name_th" type="text" class="form-control" placeholder="Name TH" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Category Name EN</label>
        <input value="<?=$category_name_en?>" name="category_name_en" type="text" class="form-control" placeholder="Name EN" required>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label>Image</label>
        <input name="category_path" onchange="readURL(this,'p_img')" type="file" class="form-control custom-file-input" <?=$required ?>>
      </div>
    </div>
    <div class="col-md-6">
      <div id="p_img">
        <?php if($action == 'EDIT'){ ?>
        <img width="100" src="upload/<?=$category_path?>">
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
<button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
<?php if($action != "SHOW"){ ?>
<button type="submit" style="width:100px;" class="btn btn-primary btn-flat">บันทึก</button>
<?php } ?>
</div>

$(function () {
    showTable();
})


function showTable(){
  $.get( "ajax/showCategory.php")
  .done(function( data ) {
    $("#showTable").html( data );
  });
}

function formSeqImage()
{
  var slideObj = $('input[name="slideID[]"]');
  var slideID  = [];
  if(slideObj.length > 0){
    slideObj.each(function() {
      slideID.push($(this).val());
    });
  }
  //console.log(slideID);
  $.post("ajax/seqImage.php",{slideID:slideID})
  .done(function( data ) {
    $.smkProgressBar({
      element:'body',
      status:'start',
      bgColor: '#000',
      barColor: '#fff',
      content: 'Loading...'
    });
    setTimeout(function(){
      $.smkProgressBar({status:'end'});
      showTable();
      showSlidebar();
      $.smkAlert({text: data.message,type: data.status});
    }, 1000);
  });
}



function deleteImage(id){

  $.post("ajax/deleteImage.php",{id:id})
    .done(function( data ) {
      showTable();
  });
}

function showForm(value){
  $.post("ajax/form.php",{value:value})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function editImage(id)
{
  $.post("ajax/form.php",{value:"EDIT",category_id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}


$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    $.ajax({
        url: 'ajax/AEDModule.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#000',
        barColor: '#fff',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});

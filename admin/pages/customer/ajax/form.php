<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$name           = "";
$lname          = "";
$email          = "";
$username       = "";
$password       = "";
$mobile         = "";
$image          = "";
$uuid           = "";


$is_active  = "";
$disabled   = "";
$disabled2  = "disabled";
$bg         = "background-color:#fff;";
$display    = "";

if($action == 'ADD')
{
  $disabled2  = "";
}

if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM  t_customer WHERE cus_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $name       = $row[0]['name'];
  $lname      = $row[0]['lname'];
  $email      = $row[0]['email'];
  $mobile     = $row[0]['mobile'];
  $wallet     = $row[0]['wallet'];
  $username   = $row[0]['username'];
  $is_active  = $row[0]['is_active'];
}

?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="cus_id" value="<?=$id?>">
<div class="modal-body">
  <!-- <div class="box"> -->
    <!-- <div class="box-header with-border">
      <h3 class="box-title"><b>ข้อมูลส่วนตัว</b></h3>
    </div> -->
    <!-- /.box-header -->
      <!-- <div class="box-body"> -->
        <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>ชื่อ</label>
                <input type="text" <?= $disabled ?> value="<?= $name ?>" name="name" class="form-control"  data-smk-msg="ระบุชื่อ" placeholder="ชื่อ" required="">
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>นามสกุล</label>
                <input type="text" <?= $disabled ?> value="<?= $lname ?>" name="lname" class="form-control" data-smk-msg="ระบุนามสกุล" placeholder="นามสกุล" required="">
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>สถานะ</label>
                <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                  <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
                  <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>Wallet</label>
                <input value="<?=@$wallet?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="wallet" type="text"  data-smk-msg="&nbsp;" class="form-control" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>เบอร์โทรศัพท์มือถือ</label>
                <input value="<?=@$mobile?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="mobile" type="tel" data-smk-pattern="0[1-9]{1}[0-9]{8}" data-smk-msg="&nbsp;" class="form-control" placeholder="0xxxxxxxxx" required>
              </div>
            </div>
            <div class="col-md-7">
              <div class="form-group">
                <label>อีเมล์</label>
                <input value="<?=@$email?>" <?= $disabled ?> name="Email" type="email" class="form-control" placeholder="อีเมล์" required data-smk-msg="&nbsp;">
              </div>
            </div>

            <div class="col-md-4">
              <div class="form-group">
                <label>Username</label>
                <input value="<?=@$username?>" <?= $disabled2 ?>  onblur="checkUserCode()" id="username" name="username" type="text" class="form-control" placeholder="User Login" required data-smk-msg="&nbsp;">
              </div>
            </div>
            <?php if($action == 'ADD'){ ?>
            <div class="col-md-4">
              <div class="form-group">
                <label>Password</label>
                <input value="" name="password" id="pass1" type="password" autocomplete="new-password" data-smk-msg="&nbsp;" class="form-control" placeholder="Password" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>Confirm Password</label>
                <input value="" id="pass2" type="password" class="form-control" data-smk-msg="&nbsp;" placeholder="Confirm Password" required>
              </div>
            </div>
          <?php } ?>
        </div>
        <!-- /.row -->
    <!-- </div> -->
  <!-- </div> -->
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>

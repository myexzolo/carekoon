<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$id = $_POST['id'];


  $sql   = "SELECT * FROM t_customer WHERE cus_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  $fname      = $row[0]['name']." ".$row[0]['lname'];
  $username   = $row[0]['username'];

?>
<input type="hidden" id="action" name="action" value="RESET">
<input type="hidden" name="cus_id" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-4">
      <div class="form-group">
        <label>username</label>
        <input value="<?=@$username?>" data-smk-msg="&nbsp;" type="text" class="form-control" placeholder="User Login" required disabled>
      </div>
    </div>
    <div class="col-md-8">
      <div class="form-group">
        <label>ชื่อ - สกุล</label>
        <input value="<?=@$fname?>" data-smk-msg="&nbsp;" type="text" class="form-control" placeholder="Name" required readonly>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Reset Password</label>
        <input value="" data-smk-msg="&nbsp;" name="password" id="pass1" type="password" autocomplete="new-password"  class="form-control" placeholder="Password" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>Confirm Password</label>
        <input value="" data-smk-msg="&nbsp;" name="cfm_password" id="pass2" type="password" autocomplete="new-password"  class="form-control" placeholder="Confirm Password" required>
      </div>
    </div>

  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;">บันทึก</button>
</div>

<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:50px">ลำดับ</th>
      <th>ชื่อ - สกุล</th>
      <th style="width:110px">เบอร์โทร</th>
      <th>อีเมล์</th>
      <th style="width:100px">Wallet</th>
      <th style="width:120px">username</th>
      <th style="width:100px">สถานะ</th>
      <th style="width:50px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:90px;">Re Password</th>
      <th style="width:50px;">แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT *
                 FROM  t_customer
                 where is_active not in ('D')
                 ORDER BY create_date DESC";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

        foreach ($rows as $key => $value) {
          $is_active = $value['is_active'];
          $token_noti = $value['token_noti'];
          $activeTxt  = "";
          $bg         = "";

          if($is_active == "Y")
          {
            $activeTxt = "รอเข้าระบบ";
            $bg        = "bg-yellow-active";
            if($token_noti != "")
            {
                $activeTxt = "พร้อมใช้งาน";
                $bg        = "bg-green-active";
            }
          }else if($is_active == "N"){
            $activeTxt = "ไม่ใช้งาน";
            $bg        = "bg-gray";
          }else if($is_active == "W"){
            $activeTxt = "รออนุมัติ";
            $bg        = "bg-aqua-active";
          }

          $fulName = $value['name']." ".$value['lname'];

    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="left"><?=$fulName ?></td>
      <td ><?=$value['mobile']?></td>
      <td align="left"><?=$value['email']?></td>
      <td align="right"><?=$value['wallet']?></td>
      <td align="left"><?=$value['username']?></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-eye" onclick="showForm('VIEW','<?=$value['cus_id']?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <td>
        <a class="btn_point text-yellow"><i class="fa fa-key" onclick="resetPass('<?=$value['cus_id']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['cus_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['cus_id']?>','<?=$fulName?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>

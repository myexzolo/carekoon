$(function () {
  pushMenu();
  showTable();
})


function pushMenu()
{
  if($( window ).width() < 1900){
     $('[data-toggle="push-menu"]').pushMenu('toggle');
  }
}


function resetPass(id){
  $.post("ajax/formResetPass.php",{id:id})
    .done(function( data ) {
      $('#myModalReset').modal({backdrop:'static'});
      $('#show-form-rw').html(data);
  });
}

function exportCSV(){
  $.get("ajax/exportCSV.php")
    .done(function( data ) {
      if(data.status == "success")
      {
        postURL_blank(data.path);
      }else{
        $.smkAlert({text: data.message,type: data.status});
      }
  });
}


function checkUserCode(){
  var text = $('#username').val();
  $.post("ajax/checkUserCode.php",{username:text})
  .done(function( data ) {
        console.log(data);
      if(data.status)
      {
        $.smkAlert({
          text: 'username ซ้ำ !!',
          type: 'danger',
          position:'top-center'
        });
        $('#username').val('');
        $('#username').focus();
      }
    });
  }


function showTable(){
  $.get("ajax/showTable.php")
    .done(function( data ) {
      $('#showTable').html(data);
  });
}

function showForm(action,id){
  $.post("ajax/form.php",{action:action,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function getAddress(name,value,code){
  if(value != ""){
    $.post( "sevice/getAddress.php", { name: name, value: value })
    .done(function( data ) {
      var district = $('#subdistrict').html('<option value="">ตำบล/แขวง</option>');
      if(data.status == 200){
        var selected = '';
        $('#subdistrict').html('');

        $.each(data.data , function(i, field){
          if(code != "")
          {
            selected = (field.SUBDISTRICT_CODE == code) ? 'selected' :'';
            //console.log(code,field.SUBDISTRICT_CODE);
          }else{
            selected = (i == 0) ? 'selected' :'';
          }
          $('#subdistrict').append('<option value="'+field.SUBDISTRICT_CODE+'" '+selected+'>'+field.SUBDISTRICT_NAME+'</option>');
        });
        //console.log(data);
        if(data.data != ""){
          getDistrict('SUBDISTRICT_CODE',data.data[0].SUBDISTRICT_CODE);
          $('#province').html('');
          $('#province').append('<option value="'+data.data[0].PROVINCE_CODE+'" '+selected+'>'+data.data[0].PROVINCE_NAME+'</option>');
        }
        var subdistrict = $('#subdistrict').val();
      }
    });
  }
}

function getDistrict(name,value){
  $.post( "sevice/getAddress.php", { name: name, value: value })
  .done(function( data ) {
    var district = $('#district').html('<option value="">อำเภอ/เขต</option>');
    if(data.status == 200){
      var selected = '';
      $('#district').html('');
      $.each(data.data , function(i, field){
        selected = (i == 0) ? 'selected' :'';
        $('#district').append('<option value="'+field.DISTRICT_CODE+'" '+selected+'>'+field.DISTRICT_NAME+'</option>');
      });
    }
  });
}

function checkIDCard(idCard){

  if(idCard.length == 13){
    var idcard = $("#idcard");
    $.post( "sevice/checkIdcard.php", { idCard: idCard })
    .done(function( data ) {
      if(data.status == 401){
        Swal.fire({
          type: 'error',
          title: 'เกิดข้อผิดพลาด',
          text: data.message,
        });
        // idcard.val('');
        idcard.val(idcard.val().substring(-1, idcard.val().length-1));
        // idcard.focus();
      }
    });
  }
}

function del(id,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูลผู้ใช้งาน : '+ name +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'DEL',cus_id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

function checknumber(inputs){
  var valid = /^\d{0,10}(\.\d{0,2})?$/.test(inputs.value),
      val = inputs.value;
  if(!valid){
      inputs.value = val.substring(0, val.length - 1);
  }
}

function chkNumber(ele)
{
    var vchar = String.fromCharCode(event.keyCode);
    if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
    ele.onKeyPress=vchar;
}


$('#formResetPassword').on('submit', function(event) {
  event.preventDefault();
  var action = $('#action').val();
  if ($('#formResetPassword').smkValidate()) {
      if( $.smkEqualPass('#pass1', '#pass2') ){
          $.ajax({
              url: 'ajax/AED.php',
              type: 'POST',
              data: new FormData( this ),
              processData: false,
              contentType: false,
              dataType: 'json'
          }).done(function( data ) {
            console.log(data);
            $.smkProgressBar({
              element:'body',
              status:'start',
              bgColor: '#fff',
              barColor: '#242d6d',
              content: 'Loading...'
            });
            setTimeout(function(){
              $.smkProgressBar({status:'end'});
              $('#formResetPassword').smkClear();
              showTable();
              showSlidebar();
              $.smkAlert({text: data.message,type: data.status});
              $('#myModalReset').modal('toggle');
            }, 1000);
          });
      }
  }
});


$('#formCustomer').on('submit', function(event) {
  event.preventDefault();
  var action = $('#action').val();
  if ($('#formCustomer').smkValidate()) {

    if(action == 'ADD'){
        if( $.smkEqualPass('#pass1', '#pass2') ){
          // Code here
            $.ajax({
                url: 'ajax/AED.php',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                dataType: 'json'
            }).done(function( data ) {
              $.smkProgressBar({
                element:'body',
                status:'start',
                bgColor: '#ecf0f5',
                barColor: '#242d6d',
                content: 'Loading...'
              });
              setTimeout(function(){
                $.smkProgressBar({status:'end'});
                $('#formCustomer').smkClear();
                showTable();
                showSlidebar();
                $.smkAlert({text: data.message,type: data.status});
                $('#myModal').modal('toggle');
              }, 1000);
            });
        }
    }else{
        $.ajax({
            url: 'ajax/AED.php',
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            dataType: 'json'
        }).done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#ecf0f5',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            $('#formCustomer').smkClear();
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
            $('#myModal').modal('toggle');
          }, 1000);
        });
    }
  }
});

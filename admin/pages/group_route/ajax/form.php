<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$group_route_name	    = "";
$group_route_img	    = "";
$seq                  = "";
$is_active            = "";

$btn = 'บันทึก';
$display  = "";
$disable  = "";

if($action == 'EDIT' || $action == 'VIEW' ){

  if($action == 'VIEW'){
    $display  = "display:none;";
    $disable  = "disabled";
  }

  $btn = 'บันทึก';

  $sqls   = "SELECT * FROM t_group_route WHERE group_route_id = '$id'";

  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];


  $group_route_name	    = $rows[0]['group_route_name'];
  $group_route_img	    = $rows[0]['group_route_img'];
  $seq	                = $rows[0]['seq'];
  $is_active            = $rows[0]['is_active'];
}


?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="group_route_id" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-9">
      <div class="form-group">
        <label>ประเภทการเดินรถ</label>
        <input value="<?=@$group_route_name?>" name="group_route_name" type="text" class="form-control" <?=$disable ?> placeholder="ประเภทการเดินรถ" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required <?=$disable ?> >
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ลำดับการแสดง</label>
        <input value="<?=@$seq?>" name="seq" type="text" class="form-control" <?=$disable ?> OnKeyPress="return chkNumber(this)" required>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>รูปประเภทการเดินรถ</label>
        <input name="group_route_img" <?= $disable ?> style="width:100%" type="file" class="form-control custom-file-input" onchange="readURL2(this,'group_route_img','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group" id="group_route_img" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
          <img src="../../../image/route/<?= $group_route_img ?>"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
      </div>
    </div>

  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>

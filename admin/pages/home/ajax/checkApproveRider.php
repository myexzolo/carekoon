<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

if(!isset($_SESSION))
{
    session_start();
}

$date   = date('Y/m/d');

$sql   = "SELECT count(rider_id) count FROM t_rider where is_active = 'W'";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$row        = $json['data'];
$dataCount  = $json['dataCount'];

$count  = $row[0]['count'];

header('Content-Type: application/json');
exit(json_encode(array('status' => true,'message' => $dataCount,'count'=> $count)));
?>

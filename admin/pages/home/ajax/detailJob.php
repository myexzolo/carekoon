<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$id_job  = $_POST['id_job'];

$sql  = "SELECT j.*, t.type_car_name, c.name, c.lname, c.mobile, r.FirstName,r.LastName,r.Mobile as mobile_rider
         FROM t_job j
         LEFT JOIN t_type_car t ON j.type_car = t.type_car
         LEFT JOIN t_customer c ON j.cus_id = c.cus_id
         LEFT JOIN t_rider r ON j.rider_id = r.rider_id
         WHERE j.id_job = '$id_job' ";

//echo $sql;
$query     = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];
$dataCount  = $json['dataCount'];
$rows       = $json['data'];


$is_active = $rows[0]['is_active'];
$bg = "";
$activeTxt = "";
if($is_active == "SB"){
  $activeTxt = "รอรับงาน";
  $bg        = "bg-yellow-active";
}else if($is_active == "W"){
  $activeTxt = "กำลังเดินทาง";
  $bg        = "bg-aqua-active";
}else if($is_active == "C"){
  $activeTxt = "ยกเลิก";
  $bg        = "bg-red-active";
}else if($is_active == "S"){
  $activeTxt = "สำเร็จ";
  $bg        = "bg-green-active";
}else if($is_active == "J"){
  $activeTxt = "Driver รับงาน";
  $bg        = "bg-aqua";
}else if($is_active == "E"){
  $activeTxt = "หมดเวลารับงาน";
  $bg        = "bg-black";
}

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>
      <div class="box-body">
        <table class="table table-bordered table-striped table-hover">
          <thead>
            <tr class="text-center">
              <th style="width:150px">รายการ</th>
              <th style="">รายละเอียด</th>
            </tr>

          </thead>
          <tbody>
            <tr>
              <th class="text-left">Order No.</th>
              <td><?= $rows[0]['order_number'] ?></td>
            </tr>
            <tr>
              <th class="text-left">วันที่</th>
              <td><?= DateTimeThai($rows[0]['create_date'])?></td>
            </tr>
            <tr>
              <th class="text-left">ประเภทรถ</th>
              <td><?= $rows[0]['type_car_name'] ?></td>
            </tr>
            <tr>
              <th class="text-left">ชื่อคนขับ</th>
              <td><?=$rows[0]['FirstName']." ".$rows[0]['LastName'] ?></td>
            </tr>
            <tr>
              <th class="text-left">ชื่อผู้ใช้บริการ</th>
              <td><?=$rows[0]['name']." ".$rows[0]['lname'] ?></td>
            </tr>
            <tr>
              <th class="text-left">ต้นทาง</th>
              <td><?=$rows[0]['start_text'] ?></td>
            </tr>
            <tr>
              <th class="text-left">ปลายทาง</th>
              <td><?=$rows[0]['end_text'] ?></td>
            </tr>
            <tr>
              <th class="text-left">สถานะ</th>
              <td><?=$activeTxt ?></td>
            </tr>
            <tr>
              <th class="text-left">ราคา</th>
              <td class="text-right"><?=number_format($rows[0]['price'],2); ?></td>
            </tr>
            <tr>
              <th class="text-left">ค่าธรรมเนียม</th>
              <td class="text-right"><?=number_format($rows[0]['cost'],2); ?></td>
            </tr>
            <tr>
              <th class="text-left">รายได้ Driver</th>
              <td class="text-right"><?=number_format($rows[0]['receive'],2); ?></td>
            </tr>
            <tr>
              <th class="text-left">จ่ายเงินสด</th>
              <td class="text-right"><?=number_format($rows[0]['pay_cash'],2); ?></td>
            </tr>
            <tr>
              <th class="text-left">จ่าย Wallet</th>
              <td class="text-right"><?=number_format($rows[0]['pay_wallet'],2); ?></td>
            </tr>
            <tr>
              <th class="text-left">ความพึงพอใจ</th>
              <td><?=$rows[0]['rate_job'] ?></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
      </div>
<!-- <script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script> -->

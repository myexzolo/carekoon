<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$dateStart  = $_POST['dateStart']." 00:00:00";
$dateEnd    = $_POST['dateEnd']." 23:59:59";
$type_car   = isset($_POST['type_car'])?$_POST['type_car']:"";
$is_active  = isset($_POST['is_active'])?$_POST['is_active']:"";

$con = "";
if($type_car != ""){
  $con .= " AND j.type_car = '$type_car' ";
}

if($is_active != "")
{
  $con .= " AND j.is_active = '$is_active' ";
}
?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:90px">Order No.</th>
      <th style="width:170px">วันที่</th>
      <th style="width:150px">ประเภทรถ</th>
      <th style="width:200px">ชื่อคนขับ</th>
      <th style="width:200px">ชื่อผู้ใช้บริการ</th>
      <th >ปลายทาง</th>
      <th style="width:100px">สถานะ</th>
      <th style="width:100px">รายละเอียด</th>
      <th style="width:90px">ยกเลิกงาน</th>
    </tr>
  </thead>
  <tbody>
    <?php
      $sql  = "SELECT j.*, t.type_car_name, c.name, c.lname, c.mobile, r.FirstName,r.LastName,r.Mobile as mobile_rider
               FROM t_job j
               LEFT JOIN t_type_car t ON j.type_car = t.type_car
               LEFT JOIN t_customer c ON j.cus_id = c.cus_id
               LEFT JOIN t_rider r ON j.rider_id = r.rider_id
               WHERE j.create_date between '$dateStart' and '$dateEnd' $con ORDER BY j.create_date DESC";

      //echo $sql;
      $query     = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

      if($dataCount > 0)
      {

        foreach ($rows as $key => $value) {

          $is_active = $value['is_active'];
          $bg = "";
          $disabled = "";
          $activeTxt = "";
          $onclick = "onclick=\"cancelJob('{$value['id_job']}','{$value['order_number']}')\"";
          if($is_active == "SB"){
            $activeTxt = "รอรับงาน";
            $bg        = "bg-yellow-active";
          }else if($is_active == "W"){
            $activeTxt = "กำลังเดินทาง";
            $bg        = "bg-aqua-active";
          }else if($is_active == "C"){
            $activeTxt = "ยกเลิก";
            $bg        = "bg-red-active";
            $disabled  = "text-disabled";
            $onclick   = "";
          }else if($is_active == "S"){
            $activeTxt = "สำเร็จ";
            $bg        = "bg-green-active";
            $disabled  = "text-disabled";
            $onclick   = "";
          }else if($is_active == "J"){
            $activeTxt = "Driver รับงาน";
            $bg        = "bg-aqua";
          }else if($is_active == "E"){
            $activeTxt = "หมดเวลารับงาน";
            $bg        = "bg-black";
            $onclick   = "";
          }
    ?>
    <tr class="text-center">
      <td><?=$value['order_number']?></td>
      <td ><?= DateTimeThai($value['create_date'])?></td>
      <td><?= $value['type_car_name'] ?></td>
      <td align="left"><?=$value['FirstName']." ".$value['LastName'] ?></td>
      <td align="left"><?=$value['name']." ".$value['lname'] ?></td>
      <td align="left"><?=$value['end_text'] ?></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-list" onclick="showDetail('<?=$value['id_job']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point text-red <?= $disabled ?>" <?=$onclick ?>><i class="fa fa-times-circle "></i></a>
      </td>
    </tr>
  <?php } }?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : true,
      "order": [[ 1, "desc" ]]
    });
  })
</script>

<!DOCTYPE html>
<?php
$pageName     = "หน้าหลัก";
$pageCode     = "Home";
?>
  <html>
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>ระบบ Back Office - <?=$pageName ?> </title>
      <link rel="shortcut icon" type="image/png" href="../../image/fav.png"/>
      <?php
        include("../../inc/css-header.php");
        $_SESSION["RE_URI"] = $_SERVER["REQUEST_URI"];
      ?>
      <link rel="stylesheet" href="css/home.css">
    </head>
    <body class="hold-transition skin-purple-light sidebar-mini" onload="showProcessbar();showSlidebar();">
      <div class="wrapper">
        <?php include("../../inc/header.php"); ?>

        <?php
          include("../../inc/sidebar.php");
          include('../../inc/function/mainFunc.php');
          $role_list  = $_SESSION['member'][0]['role_list'];
          $user_login = $_SESSION['member'][0]['user_login'];
          $roleArr   = explode(",",$role_list);
          $noDisplayVendor = "";
          $venDorID = "";

          $scol1 = "col-md-5";
          $scol2 = "col-md-4";
          $scol3 = "col-md-3";

          $RoleAdmin = false;
          $roleCodeArr  = explode(",",$_SESSION['ROLE_USER']['role_code']);
          // print_r($_SESSION['member']);
          if (in_array("ADM", $roleCodeArr) || in_array("SADM", $roleCodeArr)){
            $RoleAdmin = true;
          }
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>หน้าหลัก</h1>
            <ol class="breadcrumb">
              <li><a href=""><i class="fa fa-home"></i> Home</a></li>
            </ol>
          </section>
          <!-- Main content -->
          <section class="content">
            <?php //include("../../inc/boxes.php");
                $date = date('01/m/Y')." - ".date('t/m/Y');
            ?>
            <!-- Main row -->
          <section class="col-lg-12 connectedSortable ui-sortable">
            <div class="row">
              <div class="col-md-12 ">
                <?php

                if($_SESSION['ROLE_USER']['is_approve'])
                {
                ?>
                  <a class="btn btn-app btn-shortcut pull-right" style="font-size:20px;margin: 0px 0px 10px 10px;" href="../../pages/merchant">
                    <span class="badge bg-yellow" id="checkApproveMerchent">0</span>
                    <i class="fa fa-suitcase" style="font-size:30px;"></i> Merchant
                  </a>
                  <?php
                  }
                 if($_SESSION['ROLE_USER']['is_approve'])
                 {
                 ?>
                     <a class="btn btn-app btn-shortcut pull-right" style="font-size:20px;margin: 0 0 0px 10px;" href="../../pages/rider">
                       <span class="badge bg-yellow" id="checkApproveRider">0</span>
                       <i class="fa fa-cab" style="font-size:30px;"></i>
                       Driver
                     </a>
                 <?php
                 }
                ?>
              </div>
            <div class="col-md-12 ">
              <!-- Left col -->

          <!-- Custom tabs (Charts with tabs)-->
             <div class="box box-info" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
               <div class="box-header with-border" style="cursor: move;">
                 <i class="fa fa-search"></i>
                 <h3 class="box-title">ค้นหาการรับบริการ</h3>
               </div>

               <!-- /.box-header -->
               <div class="box-body">
                 <div class="row" style="margin-top:10px;">
                   <div class="col-md-3">
                     <div class="form-group">
                          <label for="date_reg">วันที่</label>
                          <input type="text" class="form-control" id="daterage"  value="<?= $date; ?>" style="width:100%;">
                      </div>
                   </div>
                   <div class="col-md-3">
                     <div class="form-group">
                          <label for="date_reg">สถานะ</label>
                          <select id="is_active" class="form-control select2" style="width: 100%;" required>
                            <option value="">ทั้งหมด</option>
                            <option value="SB">รอรับงาน</option>
                            <option value="J" >Driver รับงาน</option>
                            <option value="W" >กำลังเดินทาง</option>
                            <option value="C" >ยกเลิก</option>
                            <option value="E" >หมดเวลารับงาน</option>
                          </select>
                      </div>
                   </div>
                   <div class="col-md-4">
                     <div class="form-group">
                          <label for="date_reg">ประเภทรถ</label>
                          <select id="type_car" class="form-control select2" style="width: 100%;" required>
                            <option value="">&nbsp;</option>
                            <?php
                              $sql = "SELECT * FROM t_type_car where is_active = 'Y' ORDER BY type_car_name";
                              $query = DbQuery($sql,null);
                              $row  = json_decode($query,true);
                              if($row['dataCount'] > 0){
                                foreach ($row['data'] as $value) {
                            ?>
                            <option value="<?=$value['type_car']?>" ><?=$value['type_car_name']?></option>
                            <?php }} ?>
                          </select>
                      </div>
                   </div>
                 </div>
               </div>
               <div class="box-footer clearfix" align="center">
                 <button class="btn btn-primary" onclick="searchProject()" style="width:80px;">ค้นหา</button>
               </div>
            </div>
            <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1);">
               <div class="box-body">
                 <div id="show-form"></div>
               </div>
             </div>

             <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
               <div class="modal-dialog modal-lg" role="document">
                 <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     <h4 class="modal-title" id="myModalLabel">รายละเอียดการรับบริการ</h4>
                   </div>
                     <div id="show-detail"></div>
                 </div>
               </div>
             </div>

            <div class="box box-primary" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1); display:none;";>
              <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="ion ion-clipboard"></i>
                <h3 class="box-title">To Do List</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body"></div>
              <!-- /.box-body -->
              <div class="box-footer clearfix no-border">
                <button type="button" class="btn btn-default pull-right"><i class="fa fa-plus"></i> Add item</button>
              </div>
            </div>

            <div class="box box-info" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1); display:none;";>
              <div class="box-header ui-sortable-handle" style="cursor: move;">
                <i class="fa fa-envelope"></i>

                <h3 class="box-title">Quick Email</h3>
              </div>
              <div class="box-body">

              </div>
              <div class="box-footer clearfix">
                <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                  <i class="fa fa-arrow-circle-right"></i></button>
              </div>
            </div>
          </div>
        </section>
        <section class="col-lg-4 connectedSortable ui-sortable" style="display:none;">
          <div class="box box-info" style="box-shadow: 0px 0px 2px 0px rgba(135,133,131,1)";>
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="fa fa-user"></i>
              <h3 class="box-title"></h3>โปรไฟล์
            </div>
            <div class="box-body">
            </div>
          </div>
        </section>
          </div>
              <!-- /.row -->

        </section>
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include("../../inc/footer.php"); ?>
      </div>
      <!-- ./wrapper -->
      <?php include("../../inc/js-footer.php"); ?>
      <script src="js/home.js"></script>
      <script>
          $(".select2").select2();
      </script>
    </body>
  </html>

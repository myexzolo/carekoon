$(function () {
  // $('#dateRageShow').hide();
  // $('.connectedSortable').sortable({
  //   containment         : $('section.content'),
  //   placeholder         : 'sort-highlight',
  //   connectWith         : '.connectedSortable',
  //   handle              : '.box-header, .nav-tabs',
  //   forcePlaceholderSize: true,
  //   zIndex              : 999999
  // });
  // $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');
  //
  // // jQuery UI sortable for the todo list
  // $('.todo-list').sortable({
  //   placeholder         : 'sort-highlight',
  //   handle              : '.handle',
  //   forcePlaceholderSize: true,
  //   zIndex              : 999999
  // });
  //

  checkApproveMerchent();
  checkApproveRider();

  setInterval(function(){
    checkApproveMerchent();
    checkApproveRider();
  }, 3000);

  $('#daterage').daterangepicker(
    {
      locale: {
        format: 'DD/MM/YYYY',
        daysOfWeek: [
           "อา",
           "จ",
           "อ",
           "พ",
           "พฤ",
           "ศ",
           "ส"
       ],
       monthNames: [
           "มกราคม",
           "กุมภาพันธ์",
           "มีนาคม",
           "เมษายน",
           "พฤษภาคม",
           "มิถุนายน",
           "กรกฎาคม",
           "สิงหาคม",
           "กันยายน",
           "ตุลาคม",
           "พฤศจิกายน",
           "ธันวาคม"
       ]
      }
    }
  );

  searchListJob();

  $( "#daterage" ).change(function() {
    searchListJob();
  });

  $( "#type_car" ).change(function() {
    searchListJob();
  });

  $( "#is_active" ).change(function() {
    searchListJob();
  });

});


// var gdpData = {
//   "TH-57": 200,
//   "TH-56": 100,
//   "TH-55": 50,
//   "TH-54": 200
// };


function checkApproveMerchent()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/checkApproveMerchent.php")
     .done(function( data ) {
       //console.log(data);
       $("#checkApproveMerchent").html(data.count);
   });
}


function showDetail(id){
  $.post("ajax/detailJob.php",{id_job:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-detail').html(data);
  });
}


function checkApproveRider()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/checkApproveRider.php")
     .done(function( data ) {
       //console.log(data);
       $("#checkApproveRider").html(data.count);
   });
}

function searchListJob(){
  var daterage    = $('#daterage').val();
  var type_car  = $('#type_car').val();
  var is_active  = $('#is_active').val();
  var res = daterage.split("-");
  var dateStart = dateThToEn(res[0].trim(),"dd/mm/yyyy","/");
  var dateEnd = dateThToEn(res[1].trim(),"dd/mm/yyyy","/");


  $.post("ajax/searchListJob.php",{dateStart:dateStart,dateEnd:dateEnd,type_car:type_car,is_active:is_active})
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function getMap()
{
  // $.post("ajax/class.php",{week:week})
  //   .done(function( data ) {
  //     $("#showClass").html( data );
  // });
  //ปกติ #00b16a
  //กลาง #00b16a
  //เยอะ #d91e18
  $('#map').vectorMap({
    map: 'th_mill',
    backgroundColor: 'transparent',
    regionStyle: {
                    initial: {
                      fill: '#8d8d8d'
                    }
                  },
    series: {
      regions: [{
        values: gdpData,
        scale: {"200": "#d91e18","100": "#f5e51b","50": "#00b16a"},
        normalizeFunction: 'polynomial'
      }]
    },
    onRegionTipShow: function(e, el, code){
      el.html(el.html()+' (จำนวน '+gdpData[code]+' ราย)');
    }

  });
}

function showClass(week)
{
  $.post("ajax/class.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}

function showClass1(week)
{
  $.post("ajax/class1.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}


function showClass2(week)
{
  $.post("ajax/class2.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}

function cancelJob(id,name){
  $.smkConfirm({
    text:'ยืนยันการยกเลิกการรับบริการ #'+ name +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'CANCEL',id_job:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            searchListJob();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

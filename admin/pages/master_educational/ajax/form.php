<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$btn = 'บันทึก';
$display  = "";
$disable  = "";

if($action == 'EDIT' || $action == 'VIEW' ){

  if($action == 'VIEW'){
    $display  = "display:none;";
    $disable  = "disabled";
  }

  $btn = 'บันทึก';

  $sqls   = "SELECT * FROM t_educational WHERE edu_id = '$id'";

  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];


  $edu_name	 = $rows[0]['edu_name'];
  $seq	     = isset($rows[0]['seq'])?$rows[0]['seq']:0;
  $is_active = $rows[0]['is_active'];
}


?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="edu_id" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>วุฒิการศึกษา</label>
        <input value="<?=@$edu_name?>" name="edu_name" type="text" class="form-control" <?=$disable ?> placeholder="วุฒิการศึกษา" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ลำดับการแสดง</label>
        <input value="<?=@$seq?>"name="seq" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="Sequence" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active" class="form-control select2" style="width: 100%;" required <?=$disable ?> >
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
  <button type="submit" class="btn btn-primary btn-flat" style="<?=$display ?>"><?=$btn?></button>
</div>

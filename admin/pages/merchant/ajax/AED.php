<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action         = isset($_POST['action'])?$_POST['action']:"";
$merchant_id   = isset($_POST['merchant_id'])?$_POST['merchant_id']:"";



//print_r($_POST);
unset($_POST["action"]);

if($action == "ADD" || $action == "EDIT")
{
  if( $action == "EDIT")
  {
    $sql = "SELECT * FROM t_merchant WHERE merchant_id = '$merchant_id'";
    $query   = DbQuery($sql,null);
    $row     = json_decode($query,true)['data'][0];
  }
  $merchant_imgshop      = @$_FILES['merchant_imgshop'];
  $merchant_imgidcard    = @$_FILES['merchant_imgidcard'];
  $merchant_imgbookbank  = @$_FILES['merchant_imgbookbank'];


  if($merchant_imgshop['error'] == 0){
    if(isset($row['merchant_imgshop']) && $row['merchant_imgshop'] != "")
    {
      @unlink("../../../../image/merchant/".$row['merchant_imgshop']);
    }
    $s_img = uploadfile($merchant_imgshop,"../../../../image/merchant","s_");
    $_POST['merchant_imgshop'] = $s_img['image'];
  }else{
    unset($_POST["merchant_imgshop"]);
  }

  if($merchant_imgidcard['error'] == 0){
    if(isset($row['merchant_imgidcard']) && $row['merchant_imgidcard'] != "")
    {
      @unlink("../../../../image/merchant/".$row['merchant_imgidcard']);
    }
    $id_img = uploadfile($merchant_imgidcard,"../../../../image/merchant","id_");
    $_POST['merchant_imgidcard'] = $id_img['image'];
  }else{
    unset($_POST["merchant_imgidcard"]);
  }

  if($merchant_imgbookbank['error'] == 0){
    if(isset($row['merchant_imgbookbank']) && $row['merchant_imgbookbank'] != "")
    {
      @unlink("../../../../image/merchant/".$row['merchant_imgbookbank']);
    }
    $h_img = uploadfile($merchant_imgbookbank,"../../../../image/merchant","b_");
    $_POST['merchant_imgbookbank'] = $h_img['image'];
  }else{
    unset($_POST["merchant_imgbookbank"]);
  }
}

if($action == "ADD")
{
    unset($_POST["merchant_id"]);
    $sql = DBInsertPOST($_POST,'t_merchant');
}
else if($action == "EDIT")
{
    if($_POST['is_active'] == "A")
    {
        $_POST['is_active'] = "Y";
    }
    $sql = DBUpdatePOST($_POST,'t_merchant','merchant_id');
}
else if($action == "DEL")
{
    $sql = "UPDATE t_merchant SET is_active = 'D' WHERE merchant_id = '$merchant_id'";
}

//echo $sql;

$query      = DbQuery($sql,null);
$row        = json_decode($query, true);
$errorInfo  = $row['errorInfo'];

if(intval($row['errorInfo'][0]) == 0){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}



?>

<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$merchant_name      = "";
$merchant_email     = "";
$merchant_tel       = "";
$merchant_line      = "";
$merchant_address   = "";
$merchant_postCode  = "";
$merchant_subDistrictCode	 = "";
$merchant_amphorCode   = "";
$merchant_provinceCode = "";

$merchant_imgshop       = "";
$merchant_imgidcard     = "";
$merchant_imgbookbank   = "";

$IDCard     = "";
$Email      = "";
$Mobile     = "";
$Title      = "";
$FirstName  = "";
$LastName   = "";
$Address    = "";
$PostCode   = "";
$SubDistrictCode    = "";
$AmphorCode         = "";
$ProvinceCode       = "";


$merchant_lat     = "";
$merchant_lng     = "";


$is_active  = "";

$disabled   = "";
$bg         = "background-color:#fff;";
$display    = "";


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_merchant WHERE merchant_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $merchant_name      = $row[0]['merchant_name'];
  $merchant_email     = $row[0]['merchant_email'];
  $merchant_tel       = $row[0]['merchant_tel'];
  $merchant_line      = $row[0]['merchant_line'];
  $merchant_address   = $row[0]['merchant_address'];
  $merchant_postCode  = $row[0]['merchant_postCode'];
  $merchant_subDistrictCode	 = $row[0]['merchant_subDistrictCode'];
  $merchant_amphorCode   = $row[0]['merchant_amphorCode'];
  $merchant_provinceCode = $row[0]['merchant_provinceCode'];

  $merchant_imgshop       = $row[0]['merchant_imgshop'];
  $merchant_imgidcard     = $row[0]['merchant_imgidcard'];
  $merchant_imgbookbank   = $row[0]['merchant_imgbookbank'];

  $IDCard     = $row[0]['IDCard'];
  $Email      = $row[0]['Email'];
  $Mobile     = $row[0]['Mobile'];
  $Title      = $row[0]['Title'];
  $FirstName  = $row[0]['FirstName'];
  $LastName   = $row[0]['LastName'];
  $Address    = $row[0]['Address'];
  $PostCode   = $row[0]['PostCode'];
  $SubDistrictCode    = $row[0]['SubDistrictCode'];
  $AmphorCode         = $row[0]['AmphorCode'];
  $ProvinceCode       = $row[0]['ProvinceCode'];
  $merchant_type      = $row[0]['merchant_type'];
  $merchant_seq       = $row[0]['merchant_seq'];

  $merchant_lat     = $row[0]['merchant_lat'];
  $merchant_lng     = $row[0]['merchant_lng'];

  $is_active  = $row[0]['is_active'];

}

?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="merchant_id" value="<?=$id?>">
<div class="modal-body">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><b>ข้อมูลส่วนตัว</b></h3>
    </div>
    <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
            <div class="col-md-4">
              <div class="form-group">
                <label>เลขบัตรประชาชน</label>
                <input value="<?=@$IDCard?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="IDCard" type="text" class="form-control" placeholder="เลขบัตรประชาชน" required data-smk-msg="&nbsp;">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>อีเมล์</label>
                <input value="<?=@$Email?>" <?= $disabled ?> name="Email" type="email" class="form-control" placeholder="อีเมล์" required data-smk-msg="&nbsp;">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>เบอร์โทรศัพท์มือถือ</label>
                <input value="<?=@$Mobile?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="Mobile" type="tel" data-smk-pattern="0[1-9]{1}[0-9]{8}" data-smk-msg="&nbsp;" class="form-control" placeholder="0xxxxxxxxx" required>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>คำนำหน้าชื่อ</label>
                <select name="Title" class="form-control select2" style="width: 100%;" required <?= $disabled ?>>
                  <option value="">คำนำหน้าชื่อ</option>
                  <?php
                    $sql = "SELECT * FROM data_title where is_active = 'Y' ORDER BY seq,date_create";
                    $query = DbQuery($sql,null);
                    $row  = json_decode($query,true);
                    if($row['dataCount'] > 0){
                      foreach ($row['data'] as $value) {
                  ?>
                  <option value="<?=$value['title_no']?>" <?=@$Title==$value['title_no']?"selected":""?> ><?=$value['title_name']?></option>
                  <?php }} ?>
                </select>
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>ชื่อ</label>
                <input type="text" <?= $disabled ?> value="<?= $FirstName ?>" name="FirstName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="ชื่อ" required="">
              </div>
            </div>
            <div class="col-md-5">
              <div class="form-group">
                <label>นามสกุล</label>
                <input type="text" <?= $disabled ?> value="<?= $LastName ?>" name="LastName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="นามสกุล" required="">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>ที่อยู่</label>
                <input type="text" <?= $disabled ?> value="<?=$Address ?>" name="Address" class="form-control" placeholder="ที่อยู่" required="">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>รหัสไปรษณีย์</label>
                <input type="text" <?= $disabled ?> value="<?= $PostCode ?>" name="PostCode" id="postcode" minlength="5" maxlength="5" data-smk-msg="กรอกรหัสไปรษณีย์ไม่ถูกต้อง" onfocusout="getAddress('POSTCODE',this.value,'')" class="form-control" placeholder="รหัสไปรณีย์" required=""></div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>ตำบล/แขวง</label>
                <input type="hidden" value="<?= $SubDistrictCode ?>" id="subdistrict_tmp">
                <select id="subdistrict" <?= $disabled ?> style="width:100%;" name="SubDistrictCode" onfocusout="getDistrict('SUBDISTRICT_CODE',this.value)" class="form-control select2" required>
                  <option value="">ตำบล/แขวง</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>อำเภอ/เขต</label>
                <select id="district" <?= $disabled ?> name="AmphorCode" class="form-control select2" style="width:100%;"required>
                  <option value="">อำเภอ/เขต</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>จังหวัด</label>
                <select <?= $disabled ?> id="province" name="ProvinceCode" class="form-control select2" style="width:100%;" required>
                  <option value="">จังหวัด</option>
                </select>
              </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
  </div>
  <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><b>ข้อมูลร้านค้า</b></h3>
      </div>
      <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>ชื่อร้านค้า</label>
                  <input type="text" <?= $disabled ?> value="<?=$merchant_name ?>" name="merchant_name" class="form-control" placeholder="ชื่อร้านค้า" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>อีเมล์</label>
                  <input value="<?=@$merchant_email?>" <?= $disabled ?> name="merchant_email" type="email" class="form-control" placeholder="อีเมล์" required data-smk-msg="&nbsp;">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>เบอร์โทรศัพท์ร้านค้า</label>
                  <input value="<?=@$merchant_tel?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="merchant_tel" type="tel" data-smk-pattern="0[1-9]{1}[0-9]{8}" data-smk-msg="&nbsp;" class="form-control" placeholder="0xxxxxxxxx" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>ไลน์ร้านค้า</label>
                  <input type="text" <?= $disabled ?> value="<?=$merchant_line ?>" name="merchant_line" class="form-control" placeholder="Line" required>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Latitude</label>
                  <input type="text" <?= $disabled ?> value="<?=$merchant_lat ?>" name="merchant_lat" class="form-control" placeholder="Latitude" >
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>Longitude</label>
                  <input type="text" <?= $disabled ?> value="<?=$merchant_lng ?>" name="merchant_lng" class="form-control" placeholder="Longitude" >
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>ลำดับการแสดง</label>
                  <input type="text" <?= $disabled ?> OnKeyPress="return chkNumber(this)" value="<?=$merchant_seq ?>" name="merchant_seq" class="form-control text-right" placeholder="" required>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>ประเภทร้านค้า</label>
                  <select <?= $disabled ?> name="merchant_type" class="form-control select2" style="width:100%;"required>
                    <option value="">ประเภทร้านค้า</option>
                    <option value="F" <?=@$merchant_type=='F'?"selected":""?>>สินค้าและบริการ</option>
                    <option value="M" <?=@$merchant_type=='M'?"selected":""?>>E-mail Notify</option>
                  </select>
                </div>
              </div>
              <div class="col-md-9">
                <div class="form-group">
                  <label>ที่อยู่</label>
                  <input type="text" <?= $disabled ?> value="<?=$merchant_address ?>" name="merchant_address" class="form-control" placeholder="ที่อยู่" required="">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>รหัสไปรษณีย์</label>
                  <input type="text" <?= $disabled ?> value="<?= $merchant_postCode ?>" name="merchant_postCode" id="merchant_postCode" minlength="5" maxlength="5" data-smk-msg="กรอกรหัสไปรษณีย์ไม่ถูกต้อง" onfocusout="getAddress2('POSTCODE',this.value,'')" class="form-control" placeholder="รหัสไปรณีย์" required=""></div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>ตำบล/แขวง</label>
                  <input type="hidden" value="<?= $merchant_subDistrictCode ?>" id="merchant_subdistrict_tmp">
                  <select id="merchant_subdistrict" <?= $disabled ?> style="width:100%;" name="merchant_subDistrictCode" onfocusout="getDistrict('SUBDISTRICT_CODE',this.value)" class="form-control select2" required>
                    <option value="">ตำบล/แขวง</option>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>อำเภอ/เขต</label>
                  <select id="merchant_amphor" <?= $disabled ?> name="merchant_amphorCode" class="form-control select2" style="width:100%;"required>
                    <option value="">อำเภอ/เขต</option>
                  </select>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>จังหวัด</label>
                  <select <?= $disabled ?> id="merchant_province" name="merchant_provinceCode" class="form-control select2" style="width:100%;" required>
                    <option value="">จังหวัด</option>
                  </select>
                </div>
              </div>
          </div>
          <!-- /.row -->
      </div>
  </div>
  <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><b>เอกสารแนบ</b></h3>
      </div>
      <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>รูปร้านค้า</label>
                  <input name="merchant_imgshop" <?= $disabled ?> style="width:100%" type="file" class="form-control custom-file-input" onchange="readURL2(this,'merchant_imgshop','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group" id="merchant_imgshop" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
                    <img src="../../../image/merchant/<?= $merchant_imgshop ?>"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>สำเนาบัตรประชาชน</label>
                  <input name="merchant_imgidcard" <?= $disabled ?> style="width:100%" type="file" class="form-control custom-file-input" onchange="readURL2(this,'merchant_imgidcard','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group" id="merchant_imgidcard" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
                    <img src="../../../image/merchant/<?= $merchant_imgidcard ?>"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>สำเนาสมุดบัญชีธนาคาร (รูปหน้าแรก)</label>
                  <input name="merchant_imgbookbank" <?= $disabled ?> style="width:100%" type="file" class="form-control custom-file-input" onchange="readURL2(this,'merchant_imgbookbank','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group" id="merchant_imgbookbank" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
                    <img src="../../../image/merchant/<?= $merchant_imgbookbank ?>"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
                </div>
              </div>
          </div>
          <!-- /.row -->
      </div>
  </div>
  <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><b>สถานะ</b></h3>
      </div>
        <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                    <?php
                    if($is_active == "W")
                    {
                    ?>
                      <option value="">&nbsp;</option>
                      <option value="A" <?=@$is_active=='Y'?"selected":""?>>อนุมัติ</option>
                      <option value="D" <?=@$is_active=='D'?"selected":""?>>ไม่อนุมัติ</option>
                    <?php
                  }else{
                    ?>
                    <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
                    <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
                  <?php } ?>
                  </select>
                </div>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
    var postcode        = $('#postcode').val();
    var subdistrict_tmp = $('#subdistrict_tmp').val();

    var merchant_postCode         = $('#merchant_postCode').val();
    var merchant_subdistrict_tmp  = $('#merchant_subdistrict_tmp').val();

    if(postcode != ""){
      getAddress('POSTCODE', postcode, subdistrict_tmp);
    }

    if(subdistrict_tmp != ""){
      getDistrict('SUBDISTRICT_CODE',subdistrict_tmp);
    }

    if(merchant_postCode != ""){
      getAddress2('POSTCODE', merchant_postCode, merchant_subdistrict_tmp);
    }

    if(merchant_subdistrict_tmp != ""){
      getDistrict2('SUBDISTRICT_CODE',merchant_subdistrict_tmp);
    }
  })
</script>

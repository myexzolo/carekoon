$(function () {
  pushMenu();
  showTable();
})


function pushMenu()
{
  if($( window ).width() < 1900){
     $('[data-toggle="push-menu"]').pushMenu('toggle');
  }
}


function showTable(){
  $.get("ajax/showTable.php")
    .done(function( data ) {
      $('#showTable').html(data);
  });
}

function showForm(action,id){
  $.post("ajax/form.php",{action:action,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}

function showTransfer(id){
  $.post("ajax/formTransfer.php",{id:id})
    .done(function( data ) {
      $('#myModal2').modal({backdrop:'static'});
      $('#showTransfer').html(data);
  });
}

function exportCSV(){
  $.get("ajax/exportCSV.php")
    .done(function( data ) {
      if(data.status == "success")
      {
        postURL_blank(data.path);
      }else{
        $.smkAlert({text: data.message,type: data.status});
      }
  });
}


function getAddress(name,value,code){
  if(value != ""){
    $.post( "sevice/getAddress.php", { name: name, value: value })
    .done(function( data ) {
      var district = $('#subdistrict').html('<option value="">ตำบล/แขวง</option>');
      if(data.status == 200){
        var selected = '';
        $('#subdistrict').html('');

        $.each(data.data , function(i, field){
          if(code != "")
          {
            selected = (field.SUBDISTRICT_CODE == code) ? 'selected' :'';
            //console.log(code,field.SUBDISTRICT_CODE);
          }else{
            selected = (i == 0) ? 'selected' :'';
          }
          $('#subdistrict').append('<option value="'+field.SUBDISTRICT_CODE+'" '+selected+'>'+field.SUBDISTRICT_NAME+'</option>');
        });
        //console.log(data);
        if(data.data != ""){
          getDistrict('SUBDISTRICT_CODE',data.data[0].SUBDISTRICT_CODE);
          $('#province').html('');
          $('#province').append('<option value="'+data.data[0].PROVINCE_CODE+'" '+selected+'>'+data.data[0].PROVINCE_NAME+'</option>');
        }
        var subdistrict = $('#subdistrict').val();
      }
    });
  }
}

function getAddress2(name,value,code){
  if(value != ""){
    $.post( "sevice/getAddress.php", { name: name, value: value })
    .done(function( data ) {
      var district = $('#merchant_subdistrict').html('<option value="">ตำบล/แขวง</option>');
      if(data.status == 200){
        var selected = '';
        $('#merchant_subdistrict').html('');

        $.each(data.data , function(i, field){
          if(code != "")
          {
            selected = (field.SUBDISTRICT_CODE == code) ? 'selected' :'';
            //console.log(code,field.SUBDISTRICT_CODE);
          }else{
            selected = (i == 0) ? 'selected' :'';
          }
          $('#merchant_subdistrict').append('<option value="'+field.SUBDISTRICT_CODE+'" '+selected+'>'+field.SUBDISTRICT_NAME+'</option>');
        });
        //console.log(data);
        if(data.data != ""){
          getDistrict2('SUBDISTRICT_CODE',data.data[0].SUBDISTRICT_CODE);
          $('#merchant_province').html('');
          $('#merchant_province').append('<option value="'+data.data[0].PROVINCE_CODE+'" '+selected+'>'+data.data[0].PROVINCE_NAME+'</option>');
        }
        var merchant_subdistrict = $('#merchant_subdistrict').val();
      }
    });
  }
}

function getDistrict(name,value){
  $.post( "sevice/getAddress.php", { name: name, value: value })
  .done(function( data ) {
    var district = $('#district').html('<option value="">อำเภอ/เขต</option>');
    if(data.status == 200){
      var selected = '';
      $('#district').html('');
      $.each(data.data , function(i, field){
        selected = (i == 0) ? 'selected' :'';
        $('#district').append('<option value="'+field.DISTRICT_CODE+'" '+selected+'>'+field.DISTRICT_NAME+'</option>');
      });
    }
  });
}

function getDistrict2(name,value){
  $.post( "sevice/getAddress.php", { name: name, value: value })
  .done(function( data ) {
    var district = $('#merchant_amphor').html('<option value="">อำเภอ/เขต</option>');
    if(data.status == 200){
      var selected = '';
      $('#merchant_amphor').html('');
      $.each(data.data , function(i, field){
        selected = (i == 0) ? 'selected' :'';
        $('#merchant_amphor').append('<option value="'+field.DISTRICT_CODE+'" '+selected+'>'+field.DISTRICT_NAME+'</option>');
      });
    }
  });
}




function checkIDCard(idCard){

  if(idCard.length == 13){
    var idcard = $("#idcard");
    $.post( "sevice/checkIdcard.php", { idCard: idCard })
    .done(function( data ) {
      if(data.status == 401){
        Swal.fire({
          type: 'error',
          title: 'เกิดข้อผิดพลาด',
          text: data.message,
        });
        // idcard.val('');
        idcard.val(idcard.val().substring(-1, idcard.val().length-1));
        // idcard.focus();
      }
    });
  }
}

function del(id,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล Driver : '+ name +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'DEL',merchant_id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#fff',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

function checknumber(inputs){
  var valid = /^\d{0,10}(\.\d{0,2})?$/.test(inputs.value),
      val = inputs.value;
  if(!valid){
      inputs.value = val.substring(0, val.length - 1);
  }
}

function chkNumber(ele)
{
    var vchar = String.fromCharCode(event.keyCode);
    if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
    ele.onKeyPress=vchar;
}


$('#formDriver').on('submit', function(event) {
  event.preventDefault();
  if ($('#formDriver').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formDriver').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});


$('#formTransfer').on('submit', function(event) {
  event.preventDefault();
  if ($('#formTransfer').smkValidate()) {
    $.ajax({
        url: 'ajax/AED_T.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formTransfer').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal2').modal('toggle');
      }, 1000);
    });
  }
});

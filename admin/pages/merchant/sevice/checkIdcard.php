<?php

include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');


$status = 200;
$message = 'success';
$data = array('status' => true);
$idCard       = $_POST['idCard'];
if(!idCard($idCard)){
  $status = 401;
  $message = 'หมายเลขบัตรประชาชนไม่ถูกต้อง !';
  $data['status'] = false;
}
else if(duplicateIdCard($idCard))
{
  $status = 401;
  $message = 'หมายเลขบัตรประชาชนนี้ได้สมัครสมาชิกแล้ว !';
  $data['status'] = false;
}

function duplicateIdCard($idCard)
{
  $sql = "SELECT * FROM t_rider WHERE IDCard = '$idCard'";
  $query   = DbQuery($sql,null);
  $count   = json_decode($query,true)['dataCount'];

  if($count > 0){
    return true;
  }else{
    return false;
  }
}


header('Content-Type: application/json');
exit( json_encode( array('status' => $status , 'message' => $message , 'data' => $data) ) );

?>

var dataEditor;


$(function () {
  showForm();
  $('.select2').select2();
});

function showForm(){
  $.get("ajax/form.php")
    .done(function( data ) {
      $('#show-form').html(data);



      ClassicEditor
    		.create( document.querySelector( '#editor' ), {
            image: {
              styles: [
                'alignLeft',
                'alignCenter',
                'alignRight',
              ],
              resizeOptions: [
                    {
                        name: 'imageResize:original',
                        label: 'Original',
                        value: null
                    },
                    {
                        name: 'imageResize:50',
                        label: '50%',
                        value: '50'
                    },
                    {
                        name: 'imageResize:75',
                        label: '75%',
                        value: '75'
                    }
                ],
              toolbar: [
                    'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
                    '|',
                    'imageTextAlternative'
                ]
            },
            table: {
                contentToolbar: [
                    'tableColumn', 'tableRow', 'mergeTableCells'
                ]
            },
            ckfinder: {
        			// eslint-disable-next-line max-len
        			uploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
            }
        } )
    		.then( editor => {
    			dataEditor = editor;
    		} )
    		.catch( err => {
    			console.error( err.stack );
    		} );
  });
}

$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    var detail = dataEditor.getData();
    console.log(detail);
    $('#detail').val(detail);
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
      }, 1000);
    });
  }
});

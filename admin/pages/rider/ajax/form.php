<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$IDCard     = "";
$Email      = "";
$Mobile     = "";
$Birthday   = "";
$Title      = "";
$FirstName  = "";
$LastName   = "";
$NickName   = "";
$Address    = "";
$PostCode   = "";
$SubDistrictCode   = "";
$AmphorCode   = "";
$Occp       = "";
$Line       = "";
$edu_id     = "";

$type_driver    = "";
$driver_license = "";
$Type_car       = "";
$Car_no         = "";
$Car_color      = "";
$Car_year       = "";
$Car_brand      = "";
$Car_model      = "";
$Car_people     = "";

$Emergency_firstName  = "";
$Emergency_lastName   = "";
$Emergency_relation   = "";
$Emergency_Mobile     = "";

$img_profile    = "";
$img_idcard     = "";
$img_home       = "";
$img_driver_id  = "";
$img_copy_car   = "";
$img_act        = "";
$img_bookbank   = "";

$is_active  = "";

$disabled   = "";
$bg         = "background-color:#fff;";
$display    = "";


if($action == 'EDIT' || $action == 'VIEW')
{
  if($action == 'VIEW')
  {
    $disabled = "disabled";
    $bg       = "";
    $display    = "display:none;";
  }

  $sql   = "SELECT * FROM t_rider WHERE rider_id = '$id'";

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];
  $dataCount  = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;
  $IDCard     = $row[0]['IDCard'];
  $Email      = $row[0]['Email'];
  $Mobile     = $row[0]['Mobile'];
  $Birthday   = $row[0]['Birthday'];
  $Title      = $row[0]['Title'];
  $FirstName  = $row[0]['FirstName'];
  $LastName   = $row[0]['LastName'];
  $NickName   = $row[0]['NickName'];
  $Address    = $row[0]['Address'];
  $PostCode   = $row[0]['PostCode'];
  $SubDistrictCode   = $row[0]['SubDistrictCode'];
  $AmphorCode   = $row[0]['AmphorCode'];
  $Occp       = $row[0]['Occp'];
  $Line       = $row[0]['Line'];
  $edu_id     = $row[0]['edu_id'];


  $type_driver    = $row[0]['type_driver'];
  $driver_license = $row[0]['driver_license'];
  $Type_car       = $row[0]['Type_car'];
  $Car_no         = $row[0]['Car_no'];
  $Car_color      = $row[0]['Car_color'];
  $Car_year       = $row[0]['Car_year'];
  $Car_brand      = $row[0]['Car_brand'];
  $Car_model      = $row[0]['Car_model'];
  $Car_people     = $row[0]['Car_people'];

  $Emergency_firstName  = $row[0]['Emergency_firstName'];
  $Emergency_lastName   = $row[0]['Emergency_lastName'];
  $Emergency_relation   = $row[0]['Emergency_relation'];
  $Emergency_Mobile     = $row[0]['Emergency_Mobile'];


  $img_profile    = $row[0]['img_profile'];
  $img_idcard     = $row[0]['img_idcard'];
  $img_home       = $row[0]['img_home'];
  $img_driver_id  = $row[0]['img_driver_id'];
  $img_copy_car   = $row[0]['img_copy_car'];
  $img_act        = $row[0]['img_act'];
  $img_bookbank   = $row[0]['img_bookbank'];


  $is_active  = $row[0]['is_active'];


  //$user_img     = isset($row[0]['user_img'])?"../../image/user/".$row[0]['user_img']:"";
  // $branch_code  = $row[0]['branch_code'];
  // $department_id = $row[0]['department_id'];
  // if(!empty($role_list)){
  //     $arr_role_list = explode(",",$role_list);
  // }

}

?>
<input type="hidden" id="action" name="action" value="<?=$action?>">
<input type="hidden" name="rider_id" value="<?=$id?>">
<div class="modal-body">
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title"><b>ข้อมูลส่วนตัว</b></h3>
    </div>
    <!-- /.box-header -->
      <div class="box-body">
        <div class="row">
            <div class="col-md-3">
              <div class="form-group">
                <label>เลขบัตรประชาชน</label>
                <input value="<?=@$IDCard?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="IDCard" type="text" class="form-control" placeholder="เลขบัตรประชาชน" required data-smk-msg="&nbsp;">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>อีเมล์</label>
                <input value="<?=@$Email?>" <?= $disabled ?> name="Email" type="email" class="form-control" placeholder="อีเมล์" required data-smk-msg="&nbsp;">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>เบอร์โทรศัพท์มือถือ</label>
                <input value="<?=@$Mobile?>" <?= $disabled ?> OnKeyPress="return chkNumber(this)" name="Mobile" type="tel" data-smk-pattern="0[1-9]{1}[0-9]{8}" data-smk-msg="&nbsp;" class="form-control" placeholder="0xxxxxxxxx" required>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>วันเกิด</label>
                <input data-smk-msg="&nbsp;" <?= $disabled ?> class="form-control datepicker" value="<?= @DateThai($Birthday) ?>" required name="Birthday" type="text" data-provide="datepicker" data-date-language="th-th"  style="<?=$bg ?>">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>คำนำหน้าชื่อ</label>
                <select name="Title" class="form-control select2" style="width: 100%;" required <?= $disabled ?>>
                  <option value="">คำนำหน้าชื่อ</option>
                  <?php
                    $sql = "SELECT * FROM data_title where is_active = 'Y' ORDER BY seq,date_create";
                    $query = DbQuery($sql,null);
                    $row  = json_decode($query,true);
                    if($row['dataCount'] > 0){
                      foreach ($row['data'] as $value) {
                  ?>
                  <option value="<?=$value['title_no']?>" <?=@$Title==$value['title_no']?"selected":""?> ><?=$value['title_name']?></option>
                  <?php }} ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>ชื่อ</label>
                <input type="text" <?= $disabled ?> value="<?= $FirstName ?>" name="FirstName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="ชื่อ" required="">
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>นามสกุล</label>
                <input type="text" <?= $disabled ?> value="<?= $LastName ?>" name="LastName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="นามสกุล" required="">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>ชื่อเล่น</label>
                <input type="text" <?= $disabled ?> value="<?= $NickName ?>" name="NickName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="ชื่อเล่น" required="">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>ที่อยู่</label>
                <input type="text" <?= $disabled ?> value="<?=$Address ?>" name="Address" class="form-control" placeholder="ที่อยู่ Driver" required="">
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <label>รหัสไปรษณีย์</label>
                <input type="text" <?= $disabled ?> value="<?= $PostCode ?>" name="PostCode" id="postcode" minlength="5" maxlength="5" data-smk-msg="กรอกรหัสไปรษณีย์ไม่ถูกต้อง" onfocusout="getAddress('POSTCODE',this.value,'')" class="form-control" placeholder="รหัสไปรณีย์" required=""></div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>ตำบล/แขวง</label>
                <input type="hidden" value="<?= $SubDistrictCode ?>" id="subdistrict_tmp">
                <select id="subdistrict" <?= $disabled ?> style="width:100%;" name="SubDistrictCode" onfocusout="getDistrict('SUBDISTRICT_CODE',this.value)" class="form-control select2" required>
                  <option value="">ตำบล/แขวง</option>
                </select>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-group">
                <label>อำเภอ/เขต</label>
                <select id="district" <?= $disabled ?> name="AmphorCode" class="form-control select2" style="width:100%;"required>
                  <option value="">อำเภอ/เขต</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>จังหวัด</label>
                <select <?= $disabled ?> id="province" name="ProvinceCode" class="form-control select2" style="width:100%;" required>
                  <option value="">จังหวัด</option>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>อาชีพ</label>
                <select <?= $disabled ?> id="Occp" name="Occp" class="form-control select2" style="width:100%;" required>
                  <option value="">อาชีพ</option>
                  <?php
                    $sql = "SELECT * FROM t_occ where is_active = 'Y' ORDER BY seq,date_create";
                    $query = DbQuery($sql,null);
                    $row  = json_decode($query,true);
                    if($row['dataCount'] > 0){
                      foreach ($row['data'] as $value) {
                  ?>
                  <option value="<?=$value['occ_id']?>" <?=@$Occp==$value['occ_id']?"selected":""?> ><?=$value['occ_name']?></option>
                  <?php }} ?>
                </select>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>ID LINE</label>
                <input <?= $disabled ?> type="text" name="Line" value="<?=$Line ?>" class="form-control" placeholder="ID LINE" required>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                <label>วุฒิการศึกษา</label>
                <select <?= $disabled ?> id="edu_id" name="edu_id" class="form-control select2" style="width:100%;" required>
                  <option value="">วุฒิการศึกษา</option>
                  <?php
                    $sql = "SELECT * FROM t_educational where is_active = 'Y' ORDER BY seq,date_create";
                    $query = DbQuery($sql,null);
                    $row  = json_decode($query,true);
                    if($row['dataCount'] > 0){
                      foreach ($row['data'] as $value) {
                  ?>
                  <option value="<?=$value['edu_id']?>" <?=@$edu_id==$value['edu_id']?"selected":""?> ><?=$value['edu_name']?></option>
                  <?php }} ?>
                </select>
              </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
  </div>
  <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><b>ข้อมูลการขับขี่</b></h3>
      </div>
      <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>ประเภทใบอนุญาติขับขี่</label>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="radio">
                        <label>
                          <input <?= $disabled ?> type="radio" name="type_driver" value="1"  <?=@$type_driver=='1' ?"checked":""?>  required>
                          ใบขับขี่ส่วนบุคคล
                        </label>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="radio">
                        <label>
                          <input <?= $disabled ?> type="radio" name="type_driver" value="2" <?=@$type_driver=='2' ?"checked":""?>  required>
                          ใบขับขี่สาธารณะ
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group">
                  <label>เลขที่ใบขับขี่</label>
                  <input type="text" <?= $disabled ?> name="driver_license" value="<?=$driver_license ?>" class="form-control" data-smk-msg="เลขที่ใบขับขี่" placeholder="เลขที่ใบขับขี่">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                  <label>ประเภทรถ</label>
                  <select name="Type_car" <?= $disabled ?> class="form-control select2" style="width: 100%;" required>
                    <option value="">ประเภทรถ</option>
                    <?php
                      $sql = "SELECT * FROM t_type_car";
                      $query = DbQuery($sql,null);
                      $row  = json_decode($query,true);
                      if($row['dataCount'] > 0){
                        foreach ($row['data'] as $value) {
                    ?>
                    <option value="<?=$value['type_car']?>" <?=@$Type_car ==$value['type_car']?"selected":""?> ><?=$value['type_car_name']?></option>
                    <?php }} ?>
                  </select>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>ป้ายทะเบียน</label>
                  <input type="text" <?= $disabled ?> value="<?=$Car_no ?>" name="Car_no" class="form-control" data-smk-msg="ป้ายทะเบียน" placeholder="ป้ายทะเบียน" required>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>สีรถ</label>
                  <input type="text" <?= $disabled ?> value="<?=$Car_color ?>" name="Car_color" class="form-control" data-smk-msg="สีรถ" placeholder="สีรถ" required>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>ปีรถ</label>
                    <input type="text" <?= $disabled ?> value="<?=$Car_year ?>" name="Car_year" class="form-control" data-smk-msg="ปีรถยนต์" placeholder="ปีรถยนต์" required>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>ยี่ห้อรถ</label>
                <input type="text" <?= $disabled ?> value="<?=$Car_brand ?>" name="Car_brand" class="form-control" data-smk-msg="ยี่ห้อรถยนต์" placeholder="ยี่ห้อรถยนต์" required>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>รุ่นรถ</label>
                    <input type="text" <?= $disabled ?> value="<?=$Car_model ?>" name="Car_model" class="form-control" data-smk-msg="รุ่นรถยนต์" placeholder="รุ่นรถยนต์" required>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group">
                  <label>จำนวนผู้โดยสาร</label>
                  <input type="number" <?= $disabled ?> value="<?=$Car_people ?>" name="Car_people" class="form-control" data-smk-msg="จำนวนผู้โดยสาร" placeholder="จำนวนผู้โดยสาร" required>
                </div>
              </div>
          </div>
          <!-- /.row -->
      </div>
  </div>
  <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><b>ข้อมูลติดต่อกลับกรณีฉุกเฉิน</b></h3>
      </div>
      <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>ชื่อ</label>
                  <input type="text" <?= $disabled ?> value="<?=$Emergency_firstName ?>" name="Emergency_firstName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="ชื่อ" required>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>นามสกุล</label>
                  <input type="text" <?= $disabled ?> value="<?=$Emergency_lastName ?>" name="Emergency_lastName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="นามสกุล" required>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>ความสัมพันธ์</label>
                    <input type="text" <?= $disabled ?> value="<?=$Emergency_relation ?>" name="Emergency_relation" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="ความสัมพันธ์" required>
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group">
                  <label>เบอร์โทรศัพท์มือถือ</label>
                <input type="tel" <?= $disabled ?> value="<?=$Emergency_Mobile ?>" name="Emergency_Mobile" class="form-control" data-smk-pattern="0[1-9]{1}[0-9]{8}" data-smk-msg="เบอร์โทรศัพท์มือถือไม่ถูกต้อง" placeholder="0xxxxxxxxx" required>
                </div>
              </div>
          </div>
          <!-- /.row -->
      </div>
  </div>
  <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><b>เอกสารแนบ</b></h3>
      </div>
      <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>รูปถ่าย 1 นิ้ว</label>
                  <input name="img_profile" <?= $disabled ?> style="width:100%" type="file" class="form-control custom-file-input" onchange="readURL2(this,'img_profile','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group" id="img_profile" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
                    <img src="../../../image/college/<?= $img_profile ?>"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>สำเนาบัตรประชาชน</label>
                  <input name="img_idcard" <?= $disabled ?> style="width:100%" type="file" class="form-control custom-file-input" onchange="readURL2(this,'img_idcard','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group" id="img_idcard" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
                    <img src="../../../image/college/<?= $img_idcard ?>"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>สำเนาทะเบียนบ้าน</label>
                  <input name="img_home" <?= $disabled ?> style="width:100%" type="file" class="form-control custom-file-input" onchange="readURL2(this,'img_home','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group" id="img_home" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
                    <img src="../../../image/college/<?= $img_home ?>"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>สำเนาใบอนุญาตขับขี่</label>
                  <input name="img_driver_id" <?= $disabled ?> style="width:100%" type="file" class="form-control custom-file-input" onchange="readURL2(this,'img_driver_id','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group" id="img_driver_id" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
                    <img src="../../../image/college/<?= $img_driver_id ?>"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>สำเนาใบจดทะเบียนรถ/กรณีไม่ใช่เจ้าของรถ</label>
                  <input name="img_copy_car" <?= $disabled ?> style="width:100%" type="file" class="form-control custom-file-input" onchange="readURL2(this,'img_copy_car','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group" id="img_copy_car" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
                    <img src="../../../image/college/<?= $img_copy_car ?>"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>สำเนา พรบ./รายการเสียภาษี</label>
                  <input name="img_act" <?= $disabled ?> style="width:100%" type="file" class="form-control custom-file-input" onchange="readURL2(this,'img_act','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group" id="img_act" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
                    <img src="../../../image/college/<?= $img_act ?>"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <label>สำเนาสมุดบัญชีธนาคาร (รูปหน้าแรก)</label>
                  <input name="img_bookbank" <?= $disabled ?> style="width:100%" type="file" class="form-control custom-file-input" onchange="readURL2(this,'img_bookbank','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
                </div>
              </div>
              <div class="col-md-2">
                <div class="form-group" id="img_bookbank" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
                    <img src="../../../image/college/<?= $img_bookbank ?>"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
                </div>
              </div>
          </div>
          <!-- /.row -->
      </div>
  </div>
  <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title"><b>สถานะ</b></h3>
      </div>
        <div class="box-body">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <select name="is_active" <?= $disabled ?> class="form-control " style="width: 100%;" required >
                    <?php
                    if($is_active == "W")
                    {
                    ?>
                      <option value="">&nbsp;</option>
                      <option value="A" <?=@$is_active=='Y'?"selected":""?>>อนุมัติ</option>
                      <option value="D" <?=@$is_active=='D'?"selected":""?>>ไม่อนุมัติ</option>
                    <?php
                  }else{
                    ?>
                    <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
                    <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
                  <?php } ?>
                  </select>
                </div>
              </div>
          </div>
      </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
    var postcode        = $('#postcode').val();
    var subdistrict_tmp = $('#subdistrict_tmp').val();
    if(postcode != ""){
      getAddress('POSTCODE', postcode, subdistrict_tmp);
    }

    if(subdistrict_tmp != ""){
      getDistrict('SUBDISTRICT_CODE',subdistrict_tmp);
    }
  })
</script>

<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

// $action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$DateNow = date("Y/m/d");
$timeNow = date("H:s");

?>
<input type="hidden" name="rider_id" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-2">
      <div class="form-group">
        <label>ยอดเงินที่โอน</label>
        <input value="" name="amount_transfer" type="text" onkeypress="return chkNumber(this)" class="form-control" placeholder="" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>วันที่โอนเงิน</label>
        <div class='input-group' >
            <input data-smk-msg="&nbsp;" class="form-control datepicker" value="<?= @DateThai($DateNow) ?>" required name="date_transfer" type="text" data-provide="datepicker" data-date-language="th-th"  style="background-color:#fff;">
            <span class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </span>
        </div>

      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group">
        <label>เวลาที่โอนเงิน</label>
        <div class='input-group timepicker' >
            <input type='text' class="form-control" name="time_transfer" id='time_transfer'  value="<?=$timeNow?>"  style="background-color:#fff;" required />
            <span class="input-group-addon">
              <i class="fa fa-clock-o"></i>
            </span>
        </div>
      </div>
    </div>
    <div class="col-md-5">
      <div class="form-group">
        <label>ธนาคาร</label>
        <select name="bank_transfer" class="form-control select2" style="width: 100%;" required>
          <option value="">รายชื่อธนาคาร</option>
          <?php
            $sql = "SELECT * FROM t_bank";
            $query = DbQuery($sql,null);
            $row  = json_decode($query,true);
            if($row['dataCount'] > 0){
              foreach ($row['data'] as $value) {
          ?>
          <option value="<?=$value['code']?>"><?=$value['bank_name']?></option>
          <?php }} ?>
        </select>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group">
        <label>แนบหลักฐานการโอนเงิน</label>
        <input name="img_transfer" style="width:100%" type="file" required class="form-control custom-file-input" onchange="readURL2(this,'img_transfer','height=\'60px\'');" accept="image/x-png,image/gif,image/jpeg">
      </div>
    </div>
    <div class="col-md-2">
      <div class="form-group" id="img_transfer" style="margin-bottom:0px;margin-top: 15px;height: 60px;">
          <img src="../../image/no-image.png"  onerror="this.onerror='';this.src='../../image/no-image.png'" style="height: 60px;">
      </div>
    </div>

    <div class="col-md-12">
      <div class=" pull-right">
        <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
        <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
      </div>
    </div>
  </div>
</div>
<div class="modal-footer">
    <div class="box">
        <div class="box-header with-border text-left">
          <h3 class="box-title " style="font-size:20px"><b>ประวัติการโอนเงิน</b></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body text-left">
          <table class="table table-bordered table-striped table-hover" id="tableTransfer">
            <thead>
              <tr class="text-center">
                <th >ลำดับ</th>
                <th style="width:23%">วันที่และเวลาที่โอนเงิน</th>
                <th style="width:17%">ยอดเงินที่โอน</th>
                <th style="width:35%">ธนาคาร</th>
                <th style="width:17%">หลักฐานการโอนเงิน</th>
              </tr>
            </thead>
            <tbody>
              <?php
                $sqls   = "SELECT t.*, b.bank_name
                           FROM  t_transfer_wallet t, t_bank b
                           where rider_id = '$id' and t.bank_transfer = b.code order by date_create DESC";

                //echo $sqls;
                $querys     = DbQuery($sqls,null);
                $json       = json_decode($querys, true);
                $errorInfo  = $json['errorInfo'];
                $dataCount  = $json['dataCount'];
                $rows       = $json['data'];

                if($dataCount > 0){
                  foreach ($rows as $key => $value) {
              ?>
              <tr class="text-center">
                <td align="center"><?= $key+1 ?></td>
                <td align="center"><?= DateThai($value['date_transfer'])." ".$value['time_transfer'] ?></td>
                <td align="right"><?= number_format($value['amount_transfer'],2); ?></td>
                <td align="left"><?= $value['bank_name']; ?></td>
                <td align="center"><a onclick="postURLBlank('../../../image/transfer/<?= $value['img_transfer']?>')"><i class="fa fa-file-image-o"></i></a></td>
              </tr>
            <?php }} ?>
            </tbody>
          </table>
        </div>
      </div>
</div>

<script>
  var defaults = {
  calendarWeeks: true,
  showClear: true,
  showClose: true,
  allowInputToggle: true,
  useCurrent: false,
  ignoreReadonly: true,
  toolbarPlacement: 'top',
  locale: 'nl',
  icons: {
    time: 'fa fa-clock-o',
    date: 'fa fa-calendar',
    up: 'fa fa-angle-up',
    down: 'fa fa-angle-down',
    previous: 'fa fa-angle-left',
    next: 'fa fa-angle-right',
    today: 'fa fa-dot-circle-o',
    clear: 'fa fa-trash',
    close: 'fa fa-times'
  }
  };

  $(function () {
    $('.select2').select2();
    $('#tableTransfer').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });

    var optionsTime = $.extend({}, defaults, {format:'HH:mm'});
    $('.timepicker').datetimepicker(optionsTime);
  })
</script>

<?php
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

?>
<style>
  th {
    text-align: center;
    background-color: #ebebeb;
  }
</style>

<table class="table table-bordered table-striped table-hover" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <th style="width:40px">ลำดับ</th>
      <th style="width:130px">เลขบัตรประชาชน</th>
      <th>ชื่อ - สกุล</th>
      <th style="width:50px">ชื่อเล่น</th>
      <th style="width:100px">เบอร์โทร</th>
      <th>ID LINE</th>
      <th>อีเมล์</th>
      <th style="width:50px;">Wallet</th>
      <th style="width:150px">ประเภทรถ</th>
      <th style="width:100px">สถานะ</th>
      <th style="width:80px;">แจ้งโอนเงิน</th>
      <th style="width:50px;">ดู</th>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>
      <th style="width:90px;">อนุมัติ/แก้ไข</th>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <th style="width:50px;">ลบ</th>
      <?php
      }
      ?>
    </tr>
  </thead>
  <tbody>
    <?php
      $sqls   = "SELECT r.*, t.type_car_name
                 FROM t_rider r
                 LEFT JOIN t_type_car t ON r.Type_car = t.type_car
                 where r.is_active  not in ('D')
                 ORDER BY (
                 case r.is_active
                 when 'W' then 0
                 when 'Y' then 1
                 when 'N' then 2
                 end
                ), date_create DESC";

      //echo $sqls;
      $querys     = DbQuery($sqls,null);
      $json       = json_decode($querys, true);
      $errorInfo  = $json['errorInfo'];
      $dataCount  = $json['dataCount'];
      $rows       = $json['data'];

        foreach ($rows as $key => $value) {
          $TitleCode  = $value['Title'];
          $is_active  = $value['is_active'];
          $token_noti = $value['token_noti'];
          $activeTxt  = "";
          $bg         = "";
          $Title = "";
          if($TitleCode == "1")
          {
            $Title = "นาย";
          }else if($TitleCode == "2"){
            $Title = "นาง";
          }else if($TitleCode == "3"){
            $Title = "นางสาว";
          }

          if($is_active == "Y")
          {
            $activeTxt = "รอเข้าระบบ";
            $bg        = "bg-yellow-active";
            if($token_noti != "")
            {
                $activeTxt = "พร้อมใช้งาน";
                $bg        = "bg-green-active";
            }
          }else if($is_active == "N"){
            $activeTxt = "ไม่ใช้งาน";
            $bg        = "bg-gray";
          }else if($is_active == "W"){
            $activeTxt = "รออนุมัติ";
            $bg        = "bg-aqua-active";
          }

          $fulName = $Title.$value['FirstName']." ".$value['LastName'];

    ?>
    <tr class="text-center">
      <td><?=$key+1;?></td>
      <td align="center"><?=$value['IDCard']?></td>
      <td align="left"><?=$fulName ?></td>
      <td align="center"><?=$value['NickName']?></td>
      <td ><?=$value['Mobile']?></td>
      <td align="left"><?=$value['Line']?></td>
      <td align="left"><?=$value['Email']?></td>
      <td align="right"><?=$value['wallet']?></td>
      <td align="left"><?=$value['type_car_name']?></td>
      <td><div class="<?=$bg?>"><?=$activeTxt ?></div></td>
      <td>
        <a class="btn_point text-blue"><i class="fa  fa-bitcoin" onclick="showTransfer('<?=$value['rider_id']?>')"></i></a>
      </td>
      <td>
        <a class="btn_point text-green"><i class="fa fa-eye" onclick="showForm('VIEW','<?=$value['rider_id']?>')"></i></a>
      </td>
      <?php
      if($_SESSION['ROLE_USER']['is_update'])
      {
      ?>

      <td>
        <a class="btn_point"><i class="fa fa-edit" onclick="showForm('EDIT','<?=$value['rider_id']?>')"></i></a>
      </td>
      <?php
      }
      if($_SESSION['ROLE_USER']['is_delete'])
      {
      ?>
      <td>
        <a class="btn_point text-red"><i class="fa fa-trash-o" onclick="del('<?=$value['rider_id']?>','<?=$fulName?>')"></i></a>
      </td>
      <?php
      }
      ?>
    </tr>
    <?php } ?>
  </tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    });
  })
</script>

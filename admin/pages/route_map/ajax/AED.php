<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$action   = isset($_POST['action'])?$_POST['action']:"";
$id       = isset($_POST['id'])?$_POST['id']:"";

//print_r($_POST);
if($action != "DEL"){
  unset($_POST["action"]);

  $data['id']               = $id;
  $data['is_active']        = $_POST["is_active_tc"];
  $data['group_route_id']   = $_POST["group_route_id"];
  $data['route_name']       = $_POST["route_name"];


  $id_marker    = $_POST['id_marker'];
  $is_active    = $_POST['is_active'];
  $type         = $_POST['type'];
  $name         = $_POST['name'];
  $lat          = $_POST['lat'];
  $lng          = $_POST['lng'];
  $seq          = $_POST['seq'];
}


$sql = "";

if($action == "ADD")
{
    unset($data["id"]);
    $sql = DBInsertPOST($data,'t_route_map');
}
else if($action == "EDIT")
{
    //print_r($_POST);
    $sql = DBUpdatePOST($data,'t_route_map','id');
}
else if($action == "DEL")
{
    $sql = "UPDATE t_route_map SET is_active = 'D' WHERE id = '$id'";

    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $errorInfo  = $json['errorInfo'];

    if(intval($errorInfo[0]) == 0){
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'success','message' => 'Success')));
    }else{
      header('Content-Type: application/json');
      exit(json_encode(array('status' => 'danger','message' => 'Fail')));
    }
}

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$errorInfo  = $json['errorInfo'];


if($action == "ADD"){
  $id   =  $json['id'];
}


$id_marker    = $_POST['id_marker'];
$is_active    = $_POST['is_active'];
$type         = $_POST['type'];
$name         = $_POST['name'];
$lat          = $_POST['lat'];
$lng          = $_POST['lng'];
$seq          = $_POST['seq'];

if(intval($json['errorInfo'][0]) == 0){

  $sql = "";
  foreach( $id_marker as $key => $value ) {
    $dataMarker['id_marker']    = $value;
    $dataMarker['is_active']    = $is_active[$key];
    $dataMarker['type']         = $type[$key];
    $dataMarker['name']         = $name[$key];
    $dataMarker['lat']          = $lat[$key];
    $dataMarker['lng']          = $lng[$key];
    $dataMarker['seq']          = $seq[$key];
    $dataMarker['route_map_id'] = $id;

    if($value == "")
    {
      unset($dataMarker["id"]);
      $sql .= DBInsertPOST($dataMarker,'t_route_marker');
    }else{
      unset($dataMarker["route_map_id"]);
      $sql .= DBUpdatePOST($dataMarker,'t_route_marker','id_marker');
    }
  }
  //echo $sql;
  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];

  if(intval($json['errorInfo'][0]) == 0){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}







// echo $sql;




?>

<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$btn = 'บันทึก';
$display  = "";
$disable  = "";

if($action == 'EDIT' || $action == 'VIEW' ){

  if($action == 'VIEW'){
    $display  = "display:none;";
    $disable  = "disabled";
  }

  $btn = 'บันทึก';

  $sqls   = "SELECT * FROM t_route_map WHERE id = '$id'";

  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];


  $route_name	    = $rows[0]['route_name'];
  $group_route_id	= $rows[0]['group_route_id'];
  $is_active      = $rows[0]['is_active'];
}


?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="id" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>เส้นทางการเดินรถ</label>
        <input value="<?=@$route_name?>" name="route_name" type="text" class="form-control" <?=$disable ?> placeholder="เส้นทางการเดินรถ" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ประเภทการเดินรถ</label>
        <select name="group_route_id" class="form-control select2" style="width: 100%;" required <?= $disable ?>>
          <option value="">เลือกประเภทการเดินรถ</option>
          <?php
            $sql = "SELECT * FROM t_group_route where is_active not in ('D') ORDER BY seq";
            $query = DbQuery($sql,null);
            $row  = json_decode($query,true);
            if($row['dataCount'] > 0){
              foreach ($row['data'] as $value) {
          ?>
          <option value="<?=$value['group_route_id']?>" <?=@$group_route_id==$value['group_route_id']?"selected":""?> ><?=$value['group_route_name']?></option>
          <?php }} ?>
        </select>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active_tc" class="form-control select2" style="width: 100%;" required <?=$disable ?> >
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title" style="font-size:20px"><b>ROOT</b></h3>
            <div class="box-tools pull-right">
              <button type="button" <?=$disable ?> onclick="addRoot()" class="btn btn-box-tool" ><i class="fa fa-plus"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="tableRoot">
              <thead>
                <tr class="text-center">
                  <th >ลบ</th>
                  <th style="width:45%">ชื่อ</th>
                  <th style="width:20%">Latitude</th>
                  <th style="width:20%">Longitude</th>
                  <th style="width:10%">ลำดับ</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $sqls   = "SELECT *
                             FROM t_route_marker
                             where is_active not in ('D') and route_map_id = '$id' and type = 'root'
                             ORDER BY seq ASC";

                  //echo $sqls;
                  $querys     = DbQuery($sqls,null);
                  $json       = json_decode($querys, true);
                  $errorInfo  = $json['errorInfo'];
                  $dataCount  = $json['dataCount'];
                  $rows       = $json['data'];
                  if($dataCount > 0){
                    foreach ($rows as $key => $value) {
                ?>
                <tr class="text-center" id="tr_<?=$value['id_marker']?>">
                  <td><a class="btn_point text-red" style="line-height:40px;<?=$display?>"><i class="fa fa-trash-o" onclick="delRoot('<?=$value['id_marker']?>')"></i></a></td>
                  <td align="center">
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="<?=$value['id_marker']?>" name="id_marker[]" type="hidden" >
                      <input value="<?=$value['type']?>"      name="type[]" type="hidden" >
                      <input value="<?=$value['is_active']?>" name="is_active[]" id="is_active_<?=$value['id_marker']?>" type="hidden" >
                      <input value="<?=$value['name']?>" name="name[]" type="text" class="form-control" <?=$disable ?> placeholder="">
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="<?=$value['lat']?>" name="lat[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="<?=$value['lng']?>"name="lng[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="<?=$value['seq']?>"name="seq[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                </tr>
              <?php }
                 }else { ?>
                <tr class="text-center">
                  <td></td>
                  <td align="center">
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="" name="id_marker[]" type="hidden" class="form-control" >
                      <input value="Y" name="is_active[]" type="hidden" >
                      <input value="root" name="type[]" type="hidden" >
                      <input value="" name="name[]" type="text" class="form-control"  <?=$disable ?> >
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="" name="lat[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="" name="lng[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="" name="seq[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
    </div>


    <div class="col-md-12">
      <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title" style="font-size:20px"><b>Waypoint</b></h3>
            <div class="box-tools pull-right">
              <button type="button" <?=$disable ?> onclick="addMarker()" class="btn btn-box-tool" ><i class="fa fa-plus"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="tableMarker">
              <thead>
                <tr class="text-center">
                  <th >ลบ</th>
                  <th style="width:45%">ชื่อ</th>
                  <th style="width:20%">Latitude</th>
                  <th style="width:20%">Longitude</th>
                  <th style="width:10%">ลำดับ</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $sqls   = "SELECT *
                             FROM t_route_marker
                             where is_active not in ('D') and route_map_id = '$id' and type = 'marker'
                             ORDER BY seq ASC";

                  //echo $sqls;
                  $querys     = DbQuery($sqls,null);
                  $json       = json_decode($querys, true);
                  $errorInfo  = $json['errorInfo'];
                  $dataCount  = $json['dataCount'];
                  $rows       = $json['data'];
                  if($dataCount > 0){
                    foreach ($rows as $key => $value) {
                ?>
                <tr class="text-center" id="tr_<?=$value['id_marker']?>">
                  <td><a class="btn_point text-red" style="line-height:40px;<?=$display?>"><i class="fa fa-trash-o" onclick="delRoot('<?=$value['id_marker']?>')"></i></a></td>
                  <td align="center">
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="<?=$value['id_marker']?>" name="id_marker[]" type="hidden" >
                      <input value="<?=$value['type']?>"      name="type[]" type="hidden" >
                      <input value="<?=$value['is_active']?>" name="is_active[]" id="is_active_<?=$value['id_marker']?>" type="hidden" >
                      <input value="<?=$value['name']?>" name="name[]" type="text" class="form-control" <?=$disable ?> placeholder="">
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="<?=$value['lat']?>" name="lat[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="<?=$value['lng']?>"name="lng[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="<?=$value['seq']?>"name="seq[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                </tr>
              <?php }
                 }else { ?>
                <tr class="text-center">
                  <td></td>
                  <td align="center">
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="" name="id_marker[]" type="hidden" class="form-control" >
                      <input value="Y" name="is_active[]" type="hidden" >
                      <input value="marker" name="type[]" type="hidden" >
                      <input value="" name="name[]" type="text" class="form-control"  <?=$disable ?> >
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="" name="lat[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="" name="lng[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="" name="seq[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>
<script>
  $(function () {
    $('.select2').select2();
  })
</script>

$(function () {
  showTable();
})


function showTable(){
  $.get("ajax/showTable.php")
    .done(function( data ) {
      $('#showTable').html(data);
  });
}

function showForm(action,id){
  $.post("ajax/form.php",{action:action,id:id})
    .done(function( data ) {
      $('#myModal').modal({backdrop:'static'});
      $('#show-form').html(data);
  });
}


function del(id,name){
  $.smkConfirm({
    text:'ยืนยันการลบข้อมูล เส้นทางการเดินรถ : '+ name +' ?',
    accept:'Yes',
    cancel:'No'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/AED.php",{action:'DEL',id:id})
        .done(function( data ) {
          $.smkProgressBar({
            element:'body',
            status:'start',
            bgColor: '#ecf0f5',
            barColor: '#242d6d',
            content: 'Loading...'
          });
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showTable();
            showSlidebar();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

function delRoot(id)
{
  $("#tr_"+id).hide();
  $("#is_active_"+id).val("D");
}

function removeRow(id)
{
  $("#"+id).remove();
}

function addRoot()
{
  var d = new Date();
  var id = d.getTime();

  var tr = "<tr class=\"text-center\" id=\""+id+"\">"
        + "<td><a class=\"btn_point text-red\" style=\"line-height:40px;\"><i class=\"fa fa-trash-o\" onclick=\"removeRow('"+id+"')\"></i></a></td>"
        + "<td align=\"center\">"
        + "<div class=\"form-group\" style=\"margin-bottom: 0px;\">"
        + "<input name=\"id_marker[]\" type=\"hidden\" class=\"form-control\">"
        + "<input value=\"Y\" name=\"is_active[]\" type=\"hidden\">"
        + "<input value=\"root\" name=\"type[]\" type=\"hidden\" >"
        + "<input value=\"\" name=\"name[]\" type=\"text\" class=\"form-control\" >"
        + "</div></td>"
        + "<td>"
        + "<div class=\"form-group\" style=\"margin-bottom: 0px;\">"
        + "<input value=\"\" name=\"lat[]\" type=\"text\" class=\"form-control text-right\" required>"
        + "</div></td>"
        + "<td>"
        + "<div class=\"form-group\" style=\"margin-bottom: 0px;\">"
        + "<input value=\"\" name=\"lng[]\" type=\"text\" onkeypress=\"return chkNumber(this)\" class=\"form-control text-right\" required>"
        + "</div></td>"
        + "<td>"
        + "<div class=\"form-group\" style=\"margin-bottom: 0px;\">"
        + "<input value=\"\" name=\"seq[]\" type=\"text\" onkeypress=\"return chkNumber(this)\" class=\"form-control text-right\" required>"
        + "</div></td></tr>";

  $('#tableRoot tr:last').after(tr);
}


function addMarker()
{
  var d = new Date();
  var id = d.getTime();

  var tr = "<tr class=\"text-center\" id=\""+id+"\">"
        + "<td><a class=\"btn_point text-red\" style=\"line-height:40px;\"><i class=\"fa fa-trash-o\" onclick=\"removeRow('"+id+"')\"></i></a></td>"
        + "<td align=\"center\">"
        + "<div class=\"form-group\" style=\"margin-bottom: 0px;\">"
        + "<input name=\"id_marker[]\" type=\"hidden\" class=\"form-control\">"
        + "<input value=\"Y\" name=\"is_active[]\" type=\"hidden\">"
        + "<input value=\"marker\" name=\"type[]\" type=\"hidden\" >"
        + "<input value=\"\" name=\"name[]\" type=\"text\" class=\"form-control\" >"
        + "</div></td>"
        + "<td>"
        + "<div class=\"form-group\" style=\"margin-bottom: 0px;\">"
        + "<input value=\"\" name=\"lat[]\" type=\"text\" class=\"form-control text-right\" required>"
        + "</div></td>"
        + "<td>"
        + "<div class=\"form-group\" style=\"margin-bottom: 0px;\">"
        + "<input value=\"\" name=\"lng[]\" type=\"text\" onkeypress=\"return chkNumber(this)\" class=\"form-control text-right\" required>"
        + "</div></td>"
        + "<td>"
        + "<div class=\"form-group\" style=\"margin-bottom: 0px;\">"
        + "<input value=\"\" name=\"seq[]\" type=\"text\" onkeypress=\"return chkNumber(this)\" class=\"form-control text-right\" required>"
        + "</div></td></tr>";

  $('#tableMarker tr:last').after(tr);
}




$('#formEdu').on('submit', function(event) {
  event.preventDefault();
  if ($('#formEdu').smkValidate()) {
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formEdu').smkClear();
        showTable();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});

function chkNumber(ele)
{
    var vchar = String.fromCharCode(event.keyCode);
    if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
    ele.onKeyPress=vchar;
}

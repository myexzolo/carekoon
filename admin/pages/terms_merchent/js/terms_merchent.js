var dataEditor;

// import Table from "../../@ckeditor/ckeditor5-table/src/table.js";
// import TableToolbar from "../../@ckeditor/ckeditor5-table/src/tabletoolbar.js";
// import TableProperties from "../../@ckeditor/ckeditor5-table/src/tableproperties.js";

$(function () {
  showForm();
  $('.select2').select2();
});

var init = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(),
		isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function() {
		var editorElement = CKEDITOR.document.getById( 'editor' );

		// :(((
		// if ( isBBCodeBuiltIn ) {
		// 	editorElement.setHtml(
		// 		'Hello world!\n\n' +
		// 		'I\'m an instance of [url=https://ckeditor.com]CKEditor[/url].'
		// 	);
		// }

		// Depending on the wysiwygarea plugin availability initialize classic or inline editor.
		if ( wysiwygareaAvailable ) {
      dataEditor = CKEDITOR.replace('editor', {
      extraPlugins: 'uploadimage,image2,autogrow',
      autoGrow_minHeight: 200,
      autoGrow_maxHeight: 600,
      autoGrow_bottomSpace: 50,
      removePlugins: 'resize',

      // Configure your file manager integration. This example uses CKFinder 3 for PHP.
      filebrowserBrowseUrl: '../../ckfinder/ckfinder.html',
      filebrowserImageBrowseUrl: '../../ckfinder/ckfinder.html?type=Images',
      // filebrowserUploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
      //filebrowserUploadUrl: 'base64',
      filebrowserImageUploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',

      image2_alignClasses: [ 'image-align-left', 'image-align-center', 'image-align-right' ],
			image2_disableResizer: true
    });
		} else {
			editorElement.setAttribute( 'contenteditable', 'true' );
			CKEDITOR.inline( 'editor' );

			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
		}
	};

	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}
		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();

function showForm(){
  $.get("ajax/form.php")
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

// function showForm(){
//   $.get("ajax/form.php")
//     .done(function( data ) {
//       $('#show-form').html(data);
//
//       ClassicEditor
//     		.create( document.querySelector( '#editor' ), {
//             image: {
//               styles: [
//                 'alignLeft',
//                 'alignCenter',
//                 'alignRight',
//               ],
//               resizeOptions: [
//                     {
//                         name: 'imageResize:original',
//                         label: 'Original',
//                         value: null
//                     },
//                     {
//                         name: 'imageResize:50',
//                         label: '50%',
//                         value: '50'
//                     },
//                     {
//                         name: 'imageResize:75',
//                         label: '75%',
//                         value: '75'
//                     }
//                 ],
//               toolbar: [
//                     'imageStyle:alignLeft', 'imageStyle:alignCenter', 'imageStyle:alignRight',
//                     '|',
//                     'imageTextAlternative'
//                 ]
//             },
//             table: {
//                 contentToolbar: [
//                     'tableColumn', 'tableRow', 'mergeTableCells'
//                 ]
//             },
//             ckfinder: {
//         			// eslint-disable-next-line max-len
//         			uploadUrl: '../../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files&responseType=json',
//             },
//             htmlEmbed: {
//             showPreviews: true,
//             sanitizeHtml: ( inputHtml ) => {
//                   // Strip unsafe elements and attributes, e.g.:
//                   // the `<script>` elements and `on*` attributes.
//                   const outputHtml = sanitize( inputHtml );
//
//                   return {
//                       html: outputHtml,
//                       // true or false depending on whether the sanitizer stripped anything.
//                       hasChanged: true
//                   };
//               }
//           }
//         } )
//     		.then( editor => {
//     			dataEditor = editor;
//     		} )
//     		.catch( err => {
//     			console.error( err.stack );
//     		} );
//   });
// }

$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    var detail = dataEditor.getData();
    console.log(detail);
    $('#detail').val(detail);
    $.ajax({
        url: 'ajax/AED.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar({
        element:'body',
        status:'start',
        bgColor: '#fff',
        barColor: '#242d6d',
        content: 'Loading...'
      });
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAdd').smkClear();
        showSlidebar();
        $.smkAlert({text: data.message,type: data.status});
      }, 1000);
    });
  }
});

<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$action = $_POST['action'];
$id     = isset($_POST['id'])?$_POST['id']:"";

$btn = 'บันทึก';
$display  = "";
$disable  = "";

if($action == 'EDIT' || $action == 'VIEW' ){

  if($action == 'VIEW'){
    $display  = "display:none;";
    $disable  = "disabled";
  }

  $btn = 'บันทึก';

  $sqls   = "SELECT * FROM t_type_car WHERE type_car = '$id'";

  $query      = DbQuery($sqls,null);
  $row        = json_decode($query, true);
  $rows       = $row['data'];


  $type_car_name	= $rows[0]['type_car_name'];
  $is_active      = $rows[0]['is_active'];
  $fee            = ($rows[0]['fee']*100);
}


?>
<input type="hidden" name="action" value="<?=$action?>">
<input type="hidden" name="type_car" value="<?=@$id?>">
<div class="modal-body">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group">
        <label>ประเภทรถบริการ</label>
        <input value="<?=@$type_car_name?>" name="type_car_name" type="text" class="form-control" <?=$disable ?> placeholder="ประเภทรถบริการ" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>ค่าธรรมเนียม (%)</label>
        <input value="<?=@$fee?>"name="fee" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="ค่าธรรมเนียม" required>
      </div>
    </div>
    <div class="col-md-3">
      <div class="form-group">
        <label>สถานะ</label>
        <select name="is_active_tc" class="form-control select2" style="width: 100%;" required <?=$disable ?> >
          <option value="Y" <?=@$is_active=='Y'?"selected":""?>>ใช้งาน</option>
          <option value="N" <?=@$is_active=='N'?"selected":""?>>ไม่ใช้งาน</option>
        </select>
      </div>
    </div>
    <div class="col-md-12">
      <div class="box box-default">
          <div class="box-header with-border">
            <h3 class="box-title" style="font-size:20px"><b>อัตราค่าบริการ</b></h3>
            <div class="box-tools pull-right">
              <button type="button" <?=$disable ?> onclick="addRate()" class="btn btn-box-tool" ><i class="fa fa-plus"></i></button>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="tableRate">
              <thead>
                <tr class="text-center">
                  <th >ลบ</th>
                  <th style="width:30%">ระยะทางเริ่มต้น (กม.)</th>
                  <th style="width:30%">ระยะทางสิ้นสุด (กม.)</th>
                  <th style="width:30%">ราคา/กม.</th>
                </tr>
              </thead>
              <tbody>
                <?php
                  $sqls   = "SELECT *
                             FROM t_rate_driver
                             where is_active not in ('D') and type_car = '$id'
                             ORDER BY min_distance ASC";

                  //echo $sqls;
                  $querys     = DbQuery($sqls,null);
                  $json       = json_decode($querys, true);
                  $errorInfo  = $json['errorInfo'];
                  $dataCount  = $json['dataCount'];
                  $rows       = $json['data'];
                  if($dataCount > 0){
                    foreach ($rows as $key => $value) {
                ?>
                <tr class="text-center" id="tr_<?=$value['id']?>">
                  <td><a class="btn_point text-red" style="line-height:40px;<?=$display?>"><i class="fa fa-trash-o" onclick="delRate('<?=$value['id']?>')"></i></a></td>
                  <td align="center">
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="<?=$value['id']?>" name="id_rate[]" type="hidden" >
                      <input value="<?=$value['is_active']?>" name="is_active[]" id="is_active_<?=$value['id']?>" type="hidden" >
                      <input value="<?=$value['min_distance']?>" name="min_distance[]" type="text" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="<?=$value['max_distance']?>" name="max_distance[]" type="text" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="<?=$value['price']?>"name="price[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                </tr>
              <?php }
                 }else { ?>
                <tr class="text-center">
                  <td></td>
                  <td align="center">
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="" name="id_rate[]" type="hidden" class="form-control" >
                      <input value="Y" name="is_active[]" type="hidden" >
                      <input value="" name="min_distance[]" type="text" class="form-control text-right"  <?=$disable ?> required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="" name="max_distance[]" type="text" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                  <td>
                    <div class="form-group" style="margin-bottom: 0px;">
                      <input value="" name="price[]" type="text" onkeypress="return chkNumber(this)" class="form-control text-right" <?=$disable ?> placeholder="" required>
                    </div>
                  </td>
                </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ยกเลิก</button>
  <button type="submit" class="btn btn-primary btn-flat" style="width:100px;<?=$display?>">บันทึก</button>
</div>

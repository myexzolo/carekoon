<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    // $status = 200;
    // $message = 'success';
    $id    = "";
    $type  = isset($request['type'])?$request['type']:"";
    unset($request['type']);
    unset($request['functionName']);


    if($type == "rider"){
      // $sql        = "SELECT * FROM t_rider WHERE rider_id = '$id'";
      $id             = $request['rider_id'];
      $img_profile    = isset($request['img_profile'])?$request['img_profile']:"";
      if($img_profile != "")
      {
         $path = "../../image/college/";
         $request['img_profile'] = Base64ToImage($img_profile,$path,"p_".$id);
      }

      $sql = DBUpdatePOST($request,'t_rider','rider_id');

    }else if($type == "user"){
      // $sql        = "SELECT * FROM t_customer WHERE cus_id = '$id'";
      $id             = $request['cus_id'];
      $image    = isset($request['image'])?$request['image']:"";
      if($image != "")
      {
         $path = "../../image/college/";
         $request['image'] = Base64ToImage($image,$path,"cus_".$id);
      }

      $sql = DBUpdatePOST($request,'t_customer','cus_id');

    }else if($type == "merchant"){
      // $sql        = "SELECT * FROM t_merchant WHERE merc_id = '$id'";
      $sql = DBUpdatePOST($request,'t_merchant','merc_id');
    }

    //echo $sql;
    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $count      = $json['dataCount'];
    $errorInfo  = $json['errorInfo'];

    if(intval($json['errorInfo'][0]) == 0){
      if($type == "rider"){
        $sql        = "SELECT * FROM t_rider WHERE rider_id = '$id'";
      }else if($type == "user"){
        $sql        = "SELECT * FROM t_customer WHERE cus_id = '$id'";
      }else if($type == "merchant"){
        $sql        = "SELECT * FROM t_merchant WHERE merc_id = '$id'";
      }

      $query      = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $count      = $json['dataCount'];
      $errorInfo  = $json['errorInfo'];

      if(intval($json['errorInfo'][0]) == 0){
        if($count > 0){
          $data = $json['data'][0];
          if(isset($data['img_profile']) && $data['img_profile'] != "")
          {
            $data['img_profile'] = "https://www.carekoon.com/image/college/".$data['img_profile'];
          }

          if(isset($data['image']) && $data['image'] != "")
          {
            $data['image'] = "https://www.carekoon.com/image/college/".$data['image'];
          }

          $status = 200;
          $message = 'success';

        }else{
          $status = 401;
          $message = 'Data Empty';
        }
      }else{
        $status = 401;
        $message = 'Error SQL';
      }
    }else{
      $status = 401;
      $message = 'Error SQL';
    }
?>

<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    $status = 200;
    $message = 'success';

    $duration   = isset($request['duration'])?$request['duration']:""; //เวลา
    $distance   = isset($request['distance'])?$request['distance']:""; //ระยะทาง
    $type_car   = isset($request['type_car'])?$request['type_car']:"";

    if($type_car != ""){
      $sql        = "SELECT * FROM t_type_car WHERE type_car = '$type_car' and is_active = 'Y'";
      $query      = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $count      = $json['dataCount'];
      $row        = $json['data'];

      if($count > 0){
        $fee        =  $row[0]['fee'];

        $sqlc       = "SELECT * FROM t_rate_driver
                       WHERE type_car = '$type_car' and is_active = 'Y'
                       ORDER BY min_distance";
        $queryc      = DbQuery($sqlc,null);
        $jsonc       = json_decode($queryc, true);
        $countc      = $jsonc['dataCount'];
        $rowc        = $jsonc['data'];

        $price_driver = 0;
        $cost         = 0;
        $receive      = 0;

        for($x=0;$x<$countc;$x++)
        {
          $min     =  $rowc[$x]['min_distance'];
          $max     =  $rowc[$x]['max_distance'];
          $price   =  $rowc[$x]['price'];

          if($distance > $max)
          {
              $dist = ($max - $min);
              $distance -= $dist;
              if($min == 0)
              {
                $price_driver += $price;
              }else{
                $price_driver += ($dist * $price);
              }
          }else{
            if($min == 0)
            {
              $price_driver += $price;
            }else{
              $price_driver += ($distance * $price);
            }
            $distance = 0;
            break;
          }
        }

        if($price_driver > 0)
        {
            $price_driver = number_format($price_driver);
            $cost = floor(($price_driver * $fee)); //ปัดลง
            $receive = number_format(($price_driver - $cost));
        }

        $status = 200;
        $message = 'success';
        $data[0]['price'] = $price_driver;
        $data[0]['cost'] = $cost;
        $data[0]['receive'] = $receive;
      }else{
        $status = 401;
        $message = 'Type fail';
      }
    }
    else
    {
      $status = 401;
      $message = 'Type Car Not Matching';
    }
?>

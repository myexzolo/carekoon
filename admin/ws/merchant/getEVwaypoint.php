<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    $status = 200;
    $message = 'success';

    $id    = isset($request['id'])?$request['id']:"";
    $type_car    = isset($request['type_car'])?$request['type_car']:"";

    $str = '';
    $str .= isset($request['id'])?"AND id ='$id'":"";
    $str .= isset($request['type_car'])?"AND type_car ='$type_car'":"";

    $sql   = "SELECT * FROM t_route_map WHERE 1=1 AND is_active = 'Y' $str";


    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $count      = $json['dataCount'];
    $errorInfo  = $json['errorInfo'];
    $row        = $json['data'];

    if(intval($json['errorInfo'][0]) == 0){
      if($count > 0){

        if(empty($str) || isset($request['type_car'])){

          foreach ($row as $key => $value) {
            $sql    = "SELECT * FROM t_route_marker WHERE is_active = 'Y' and route_map_id ='{$value['id']}'";
            $query  = DbQuery($sql,null);
            $json   = json_decode($query, true);
            $count  = $json['dataCount'];
            $row[$key]['start'] = $json['data'][0]['name'];
            $row[$key]['end'] = $json['data'][$count-1]['name'];
          }
          $data = $row;
          exit( json_encode( array('status' => 200 , 'message' => 'success' , 'data' => $data ) ) );
        }


        $data['route_name'] = $row[0]['route_name'];
        $root               = array();
        $marker             = array();

        $sqlr  = "SELECT * FROM t_route_marker WHERE is_active = 'Y' and route_map_id ='$id' order by type asc, seq asc";


        $queryr      = DbQuery($sqlr,null);
        $jsonr       = json_decode($queryr, true);
        $countr      = $jsonr['dataCount'];
        $errorInfor  = $jsonr['errorInfo'];
        $rowr        = $jsonr['data'];

        if(intval($jsonr['errorInfo'][0]) == 0){
          if($countr > 0)
          {
            $numr = 0;
            $numm = 0;
            //print_r($rowr);
            foreach ($rowr as $key => $value)
            {
                if($value['type'] == "root")
                {
                  $root[$numr]['id_marker'] = $value['id_marker'];
                  $root[$numr]['name'] = $value['name'];
                  $root[$numr]['lat'] = $value['lat'];
                  $root[$numr]['lng'] = $value['lng'];
                  $root[$numr]['seq'] = $value['seq'];
                  $numr++;
                }else if($value['type'] == "marker"){
                  $root[$numr]['id_marker'] = $value['id_marker'];
                  $marker[$numm]['name'] = $value['name'];
                  $marker[$numm]['lat'] = $value['lat'];
                  $marker[$numm]['lng'] = $value['lng'];
                  $marker[$numm]['seq'] = $value['seq'];
                  $numm++;
                }
            }
            $data['root'] = $root;
            $data['waypoint'] = $marker;
          }else{
            $status = 401;
            $message = 'Data Empty';
          }
        }else{
          $status = 401;
          $message = 'Error SQL';
        }
      }else{
        $status = 401;
        $message = 'Data Empty';
      }
    }else{
      $status = 401;
      $message = 'Error SQL';
    }
?>

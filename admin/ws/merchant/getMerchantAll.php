<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$status = 200;
$message = "Success";


$merchant_str = 'https://www.carekoon.com/image/merchant/';
$food_str = 'https://www.carekoon.com/image/food/';
$sql        = "SELECT DISTINCT merchant_type FROM t_merchant ORDER BY merchant_type DESC";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$i = 0;
if($json['dataCount'] > 0){

  foreach ($json['data'] as $value) {
    $merchant_type_name = $value['merchant_type']=="F"?"ร้านอาหาร":"ปรึกษาออนไลน์";
    $data[$i]['merchant_type'] = $value['merchant_type'];
    $data[$i]['merchant_type_name'] = $merchant_type_name;
    $sql        = "SELECT
                  * ,
                  CONCAT('$merchant_str',merchant_imgshop) AS merchant_imgshop,
                  CONCAT('$merchant_str',merchant_imgbanner) AS merchant_imgbanner,
                  CONCAT('$merchant_str',merchant_imgidcard) AS merchant_imgidcard,
                  CONCAT('$merchant_str',merchant_imgbookbank) AS merchant_imgbookbank
                  FROM t_merchant WHERE is_active IN ('Y','C') AND merchant_type = '{$value['merchant_type']}' ORDER BY merchant_seq ASC";
    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    if($json['dataCount'] > 0){
      $data[$i]['data'] = $json['data'];
      $j = 0;
      foreach ($json['data'] as $value) {
        $sql2        = "SELECT * ,
                        CONCAT('$food_str', food_img) AS food_img
                        FROM t_food WHERE merchant_id = '{$value['merchant_id']}' and is_active IN ('Y','C')";
        $query2      = DbQuery($sql2,null);
        $json2       = json_decode($query2, true);
        if($json2['dataCount'] > 0){
          $data[$i]['data'][$j]['merchant_detail'] = $json2['data'];
        }else{
          $data[$i]['data'][$j]['merchant_detail'] = array();
        }
        $j++;
      }
      $i++;
    }else{
      $data[$i]['data'] = array();
    }
  }

}else{
    $status = 401;
    $message = 'Data Empty';
}


  // $sql        = "SELECT * FROM t_merchant WHERE is_active = 'Y'";
  // $query      = DbQuery($sql,null);
  // $json       = json_decode($query, true);
  // $count      = $json['dataCount'];
  // $row        = $json['data'];
  //
  // //echo $sql;
  //
  // if($count > 0)
  // {
  //   for($i=0;$i < $count ; $i++)
  //   {
  //     if(isset($row[$i]['merchant_imgshop']) && $row[$i]['merchant_imgshop'] != "")
  //     {
  //       $row[$i]['merchant_imgshop'] = "https://www.carekoon.com/image/merchant/".$row[$i]['merchant_imgshop'];
  //     }
  //     if(isset($row[$i]['merchant_imgbanner']) && $row[$i]['merchant_imgbanner'] != "")
  //     {
  //       $row[$i]['merchant_imgbanner'] = "https://www.carekoon.com/image/merchant/".$row[$i]['merchant_imgbanner'];
  //     }
  //
  //     $data[$i]  = $row[$i];
  //
  //     $merchant_id              = $row[$i]['merchant_id'];
  //     $data[$i]['merchant_id']  = $merchant_id;
  //
  //
  //     $sql2        = "SELECT * FROM t_food WHERE merchant_id = '$merchant_id' and is_active = 'Y'";
  //     $query2      = DbQuery($sql2,null);
  //     $json2       = json_decode($query2, true);
  //     $count2      = $json2['dataCount'];
  //     $row2        = $json2['data'];
  //
  //     $data[$i]['merchant_detail'] = [];
  //     for($x=0;$x<$count2 ;$x++)
  //     {
  //       $data[$i]['merchant_detail'][$x]['food_id'] = $row2[$x]['food_id'];
  //       $data[$i]['merchant_detail'][$x]['food_name'] = $row2[$x]['food_name'];
  //       $data[$i]['merchant_detail'][$x]['food_price'] = $row2[$x]['food_price'];
  //       // $data[$i]['merchant_detail'][$x]['food_img'] = $row2[$x]['food_img'];
  //       if(isset($row2[$x]['food_img']) && $row2[$x]['food_img'] != "")
  //       {
  //         $data[$i]['merchant_detail'][$x]['food_img'] = "https://www.carekoon.com/image/food/".$row2[$x]['food_img'];
  //       }
  //       $data[$i]['merchant_detail'][$x]['food_detail'] = $row2[$x]['food_detail'];
  //     }
  //   }
  // }else{
  //   $status = 401;
  //   $message = 'Data Empty';
  // }
?>

<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');


$merchant_id  = isset($request['merchant_id'])?$request['merchant_id']:"";
$status = 200;
$message = "Success";

if($merchant_id != ""){
  $sql        = "SELECT * FROM t_merchant WHERE merchant_id = '$merchant_id' and is_active = 'Y'";
  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $count      = $json['dataCount'];
  $row        = $json['data'];

  //echo $sql;

  if($count > 0){

    $data[0]['merchant_id']    = $merchant_id;
    $data[0]['merchant_name'] = $row[0]['merchant_name'];
    $data[0]['merchant_lat']  = $row[0]['merchant_lat'];
    $data[0]['merchant_lng']  = $row[0]['merchant_lng'];
    if(isset($row[0]['merchant_imgshop']) && $row[0]['merchant_imgshop'] != "")
    {
      $data[0]['merchant_imgshop'] = "https://www.carekoon.com/image/merchant/".$row[0]['merchant_imgshop'];
    }else{
      $data[0]['merchant_imgshop'] = "";
    }
    if(isset($row[0]['merchant_imgbanner']) && $row[0]['merchant_imgbanner'] != "")
    {
      $data[0]['merchant_imgbanner'] = "https://www.carekoon.com/image/merchant/".$row[0]['merchant_imgbanner'];
    }else{
      $data[0]['merchant_imgbanner'] = "";
    }

    $sql2        = "SELECT * FROM t_food WHERE merchant_id = '$merchant_id' and is_active = 'Y'";
    $query2      = DbQuery($sql2,null);
    $json2       = json_decode($query2, true);
    $count2      = $json2['dataCount'];
    $row2        = $json2['data'];

    $data[0]['merchant_detail'] = [];
    for($x=0;$x<$count2 ;$x++)
    {
      $data[0]['merchant_detail'][$x]['food_id'] = $row2[$x]['food_id'];
      $data[0]['merchant_detail'][$x]['food_name'] = $row2[$x]['food_name'];
      $data[0]['merchant_detail'][$x]['food_price'] = $row2[$x]['food_price'];
      //$data[0]['merchant_detail'][$x]['food_img'] = $row2[$x]['food_img'];
      if(isset($row2[$x]['food_img']) && $row2[$x]['food_img'] != "")
      {
        $data[0]['merchant_detail'][$x]['food_img'] = "https://www.carekoon.com/image/food/".$row2[$x]['food_img'];
      }
      $data[0]['merchant_detail'][$x]['food_detail'] = $row2[$x]['food_detail'];
    }
  }else{
    $status = 401;
    $message = 'Data Empty';
  }
}
else
{
  $status = 401;
  $message = 'Merchant ID Empty';
}
?>

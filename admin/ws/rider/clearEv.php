<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    $id_marker        = isset($request['id_marker'])?$request['id_marker']:"";
    $id_service_ev    = isset($request['id_service_ev'])?$request['id_service_ev']:"";
    $status           = isset($request['status'])?$request['status']:"C";

    $sql = "";
    if($status == "SB")
    {
      if($id_marker != "")
      {
        $sqlm   = "SELECT * from t_route_marker where id_marker = '$id_marker'";
        //echo $sql;
        $querym      = DbQuery($sqlm,null);
        $jsonm       = json_decode($querym, true);
        $rowm        = $jsonm['data'];
        $nameMarker  = $rowm[0]['name'];



        // $serverKey = "AAAA29M7ly0:APA91bEQvumgm58C49tn52aHzeB18WOwV4GKN8RN3nqWk8gdY2qM5ssGmdRMya60_fhtoKmxNftvLBqDKHUKxNcXnhT8sO6zVok0MUd4xVjpBRJWuZAxaODZaeqpeb4iyOgCPdvAN2K0";

        $sql        = "SELECT ev.id_service_ev ,c.name, c.lname ,c.token_noti,c.line_user_id
                       FROM t_trans_service_ev ev , t_customer c
                       WHERE ev.id_marker = '$id_marker' and ev.cus_id = c.cus_id and c.is_active = 'Y' and c.line_user_id is not null and c.line_user_id != ''";
        $query      = DbQuery($sql,null);
        $json       = json_decode($query, true);
        $count      = $json['dataCount'];
        $row        = $json['data'];

        //echo $sql;
        $messages   = "รถ EV ถึงสถานี".$nameMarker." แล้ว !!!!";
        $userId     =  array();
        for($i=0;$i<$count; $i++)
        {
          $token_noti     = $row[$i]['token_noti'];
          $name           = $row[$i]['name'];
          $lname          = $row[$i]['lname'];
          $id_service_ev  = $row[$i]['id_service_ev'];
          $userId[$i]      = $row[$i]['line_user_id'];

          // $title          = "คุณ".$name." ".$lname." รถ EV ถึงสถานีแล้ว !!!!";
          // $body           = "รถ EV ถึงสถานี ".$nameMarker;
          // $message_title  = "คุณ".$name." ".$lname." รถ EV ถึงสถานีแล้ว !!!!";
          // $text           = "รถ EV ถึงสถานี ".$nameMarker;


          // $dataArray = array(
          //   "token" => $token_noti,
          //   "serverkey" => $serverKey,
          //   "messageFontApp" => array(
          //                       'title' =>$title ,
          //                       'text' => $body,
          //                       'sound' => 'default',
          //                       'badge' => '1'
          //                     ),
          //   "messageInApp" => array("message_title" => $message_title , "text" => $text, "id_service_ev" => $id_service_ev)
          // );

          //print_r($dataArray);
          // sendNotify($dataArray);
        }
        //echo count($userId)." >>>>".$userId[0];
        if(count($userId) > 0 && $userId[0] != "")
        {
          sendLineMessages($userId,$messages);
        }
        //exit();
        $status   = 200;
        $message  = 'Success';
      }
      else
      {
        $status = 401;
        $message = 'Fail ID Marker Empty';
      }
    }
    else if($status == "C")
    {
      if($id_marker != "")
      {
        $sql = "UPDATE t_trans_service_ev SET is_active = 'E' WHERE id_marker = '$id_marker' ";
      }
      else if ($id_service_ev != "")
      {
        $sql = "UPDATE t_trans_service_ev SET is_active = 'E' WHERE id_service_ev = '$id_service_ev'";
      }
      //echo $sql;
      $query      = DbQuery($sql,null);
      $row        = json_decode($query, true);
      $errorInfo  = $row['errorInfo'];

      if(intval($errorInfo[0]) == 0){
        $status   = 200;
        $message  = 'Success';
      }else{
        $status = 401;
        $message = 'Fail';
      }
    }
    else
    {
      $status = 401;
      $message = 'Fail Status Empty';
    }

?>

<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');


$id_job       = isset($request['id_job'])?$request['id_job']:"";
$rider_id     = isset($request['rider_id'])?$request['rider_id']:"";

// SB = รอรับงาน
// C = ยกเลิก
// S = สำเร็จ
// J = รับงาน
// W = กำลังเดินทาง
// E = หมดเวลารับงาน


$conDate    = date("Ym");
$sql        = "SELECT * FROM t_job
               WHERE id_job = '$id_job'
               and rider_id = '$rider_id'
               and is_active = 'W'";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$count      = $json['dataCount'];
$row        = $json['data'];

//echo $sql;
if($count > 0)
{
  $cus_id     = $row[0]['cus_id'];
  $order_number  = $row[0]['order_number'];
  $pay_wallet = isset($row[0]['pay_wallet'])?$row[0]['pay_wallet']:"0";
  $price      = $row[0]['price'];
  $cost       = $row[0]['cost'];
  $receive    = $row[0]['receive'];
  $pay_cash   = isset($row[0]['pay_cash'])?$row[0]['pay_cash']:"0";

  $sqlc  = "SELECT wallet FROM t_customer WHERE cus_id = '$cus_id'";
  $queryc       = DbQuery($sqlc,null);
  $jsonc        = json_decode($queryc, true);
  $rowc         = $jsonc['data'];
  $wallet       = isset($rowc[0]['wallet'])?$rowc[0]['wallet']:"0";

  if($pay_wallet > 0)
  {
    $wallet -= $pay_wallet;
  }

  if($wallet >= 0)
  {
    $conRider = "";
    $sql = "UPDATE t_job SET
            is_active   = 'S',
            end_date  = NOW()
            WHERE id_job = '$id_job';";

    if($pay_wallet > 0)
    {
      $sqlr  = "SELECT wallet FROM t_rider WHERE rider_id = '$rider_id'";
      $queryr       = DbQuery($sqlr,null);
      $jsonr        = json_decode($queryr, true);
      $rowr         = $jsonr['data'];
      $rider_wallet = isset($rowr[0]['wallet'])?$rowr[0]['wallet']:"0";

      $rider_wallet += $pay_wallet;

      $conRider = ", wallet = '$rider_wallet' ";
      $sql .= "UPDATE t_customer SET wallet = '$wallet' WHERE cus_id = '$cus_id';";
    }

    $sql .= "UPDATE t_rider SET status_job = 'J' $conRider WHERE rider_id = '$rider_id';";


    $query      = DbQuery($sql,null);
    $json        = json_decode($query, true);
    $errorInfo  = $json['errorInfo'];

    if(intval($errorInfo[0]) == 0){
      $status = 200;
      $message = 'Success';
      $data[0]['order_number'] = $id_job;
      $data[0]['order_number'] = $order_number;
      $data[0]['price'] = $price;
      $data[0]['cost'] = $cost;
      $data[0]['receive'] = $receive;
      $data[0]['pay_wallet'] = $pay_wallet;
      $data[0]['pay_cash'] = $pay_cash;
    }else{
      $status = 401;
      $message = 'Fail';
      // $data[0]['order_number'] = '';
      // $data[0]['rider_lat'] = '';
      // $data[0]['rider_lng'] = '';
    }
  }else{
    $status = 401;
    $message = 'จำนวน Wallet ไม่เพียงพอ';
  }
}
else
{
    $status = 401;
    $message = "End Job Fail";
    // $data[0]['order_number'] = '';
    // $data[0]['rider_lat'] = '';
    // $data[0]['rider_lng'] = '';
}
?>

<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');


$id_job       = isset($request['id_job'])?$request['id_job']:"";
$rider_id     = isset($request['rider_id'])?$request['rider_id']:"";
$lat          = isset($request['lat'])?$request['lat']:"";
$lng          = isset($request['lng'])?$request['lng']:"";

// SB = รอรับงาน
// C = ยกเลิก
// S = สำเร็จ
// J = รับงาน
// W = กำลังเดินทาง
// E = หมดเวลารับงาน

$sql        = "SELECT * FROM t_job WHERE id_job = '$id_job' and is_active = 'SB'";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$count      = $json['dataCount'];
$row        = $json['data'];

//echo $sql;
if($count > 0)
{
  $order_number = $row[0]['order_number'];
  $merchant_id  = empty($row[0]['merchant_id'])?"":$row[0]['merchant_id'];

  $sql = "UPDATE t_job SET
          is_active   = 'J',
          rider_id    = '$rider_id',
          real_lat   = '$lat',
          real_lng   = '$lng',
          rider_lat   = '$lat',
          rider_lng   = '$lng',
          get_date    = NOW()
          WHERE id_job = '$id_job';";

  $sql .= "UPDATE t_rider SET status_job = 'W' WHERE rider_id = '$rider_id';";


  $query      = DbQuery($sql,null);
  $json        = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];

  if(intval($errorInfo[0]) == 0){
    $status = 200;
    $message = 'Success';
    $data[0]['id_job']                = $id_job;
    $data[0]['order_number']          = $order_number;
    $data[0]['start_address']['lat']  = $row[0]['start_lat'];
    $data[0]['start_address']['lng']  = $row[0]['start_lng'];
    $data[0]['end_address']['lat']    = $row[0]['end_lat'];
    $data[0]['end_address']['lng']    = $row[0]['end_lng'];
    $data[0]['rider_lat'] = $lat;
    $data[0]['rider_lng'] = $lng;
    //$data[0]['merchant_id'] = $merchant_id;
    // echo ">>>>>>>>>>>>>merchant_id :".$merchant_id;
    if($merchant_id != "")
    {

      $sqlm        = "SELECT * FROM t_merchant WHERE merchant_id = '$merchant_id' and is_active = 'Y'";
      $querym      = DbQuery($sqlm,null);
      $jsonm       = json_decode($querym, true);
      $rowm        = $jsonm['data'];

      $userId      = empty($rowm[0]['merchant_userIdLine'])?"":$rowm[0]['merchant_userIdLine'];
      // echo ">>>>>>>>>>>>>userId :".$userId;
      //$data[0]['userId'] = $userId;
      if($userId != "")
      {
        include "../../webhook/function.php";

        $LINEDatas['url'] = "https://api.line.me/v2/bot/message/push";
        $LINEDatas['token'] = "iWvFetWDJ/fevPqFuxvi8Ko/L638hoBIg83FgI67shdtRfZJgpZgDVhmQAXBGby5kLvBDUT1OThBSVfxvDf8xJBBakFvX3b822OHAjPU2sY6ztcDtymKJQqx5ir2Dc8rvsqx1eYVcOjPABEsNW4mfQdB04t89/1O/w1cDnyilFU=";

        //$userId = 'Ue24ce244a84c3c5fbb9bd26f3f503efb';
        $encodeJson = array(
            'to' => $userId, // merchant_userIdLine
            'messages' => array(
              array(
                'type' => 'text',
                'text' => "Hi some people want to order's form you. Please Check Merchant's now!\nQuick link -> https://carekoon.com/page/merchant_management/?member=$userId <-"
              )
            )
          );
        $encodeJson = json_encode($encodeJson);
        $mes = sentMessage($encodeJson,$LINEDatas);
        //print_r($mes);
      }
    }
  }else{
    $status = 401;
    $message = 'Fail';
    // $data[0]['order_number'] = '';
    // $data[0]['rider_lat'] = '';
    // $data[0]['rider_lng'] = '';
  }
}
else
{
    $sql        = "SELECT * FROM t_job WHERE id_job = '$id_job'";
    $query      = DbQuery($sql,null);
    $json       = json_decode($query, true);
    $count      = $json['dataCount'];
    $row        = $json['data'];
    if($count > 0)
    {
      $order_number = $row[0]['order_number'];
      $message = "#".$order_number. ' มี Driver รับงานนี้แล้ว !!';
    }else{
      $message = "ไม่พบคำสั่งงานนี้ !!";
    }
    $status = 401;

    // $data[0]['order_number'] = '';
    // $data[0]['rider_lat'] = '';
    // $data[0]['rider_lng'] = '';
}
?>

<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    $status = 200;
    $message = 'success';

    $route_id     = isset($request['route_id'])?$request['route_id']:"";


    if($route_id != "")
    {
      $sql        = "SELECT * FROM t_route_marker
                     WHERE route_map_id = '$route_id' and is_active = 'Y' order by seq ";
      $query      = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $dataCount  = $json['dataCount'];
      $row        = $json['data'];

      if($dataCount > 0)
      {
        $num = 0;
        for($i=0;$i<$dataCount;$i++)
        {
          $name        = $row[$i]['name'];
          $id_marker   = $row[$i]['id_marker'];

          $sql2        = "SELECT s.*, c.name, c.lname
                          FROM t_trans_service_ev s, t_customer c
                          WHERE s.id_marker = '$id_marker'
                          and 	s.is_active = 'Y' and s.cus_id = c.cus_id order by s.date_create";
          $query2      = DbQuery($sql2,null);
          $json2       = json_decode($query2, true);
          $dataCount2  = $json2['dataCount'];
          $row2        = $json2['data'];

          for($x=0;$x<$dataCount2;$x++)
          {
            if($x==0)
            {
              $data[$num]['name'] = $name;
              $data[$num]['id_marker'] = $id_marker;
            }
            $data[$num]['list'][$x]['id_service_ev'] = $row2[$x]['id_service_ev'];
            $data[$num]['list'][$x]['name'] = $row2[$x]['name'];
            $data[$num]['list'][$x]['lname'] = $row2[$x]['lname'];
          }
          if(isset($data[$num]['name']))
          {
            $num++;
          }
        }
        $status   = 200;
        $message  = 'Success';
      }else{
        $status = 401;
        $message = 'Data Empty';
      }
    }
    else
    {
      $status = 401;
      $message = 'Fail';
    }
?>

<?php
include('config.php');
$status = 404;
$message = 'functionName is not macth';
$data = array();

$postdata = file_get_contents("php://input");
$request = json_decode($postdata,true);
$functionName = @$request['functionName'];
switch (@$request['functionName']) {
  //ALL
  case 'getAbout':
    include('all/getAbout.php');
    break;

  case 'getContact':
    include('all/getContact.php');
    break;

  case 'getConsent':
    include('all/getConsent.php');
    break;

  case 'getPolicy':
    include('all/getPolicy.php');
    break;

  case 'getProfile':
    include('all/getProfile.php');
    break;

  case 'editProfile':
    include('all/editProfile.php');
    break;


  case 'getCalDistance':
    include('all/getCalDistance.php');
    break;

  case 'getTakeDriver':
    include('all/getTakeDriver.php');
    break;

  case 'getTypeCar':
    include('all/getTypeCar.php');
    break;

  case 'getLocationRider':
    include('all/getLocationRider.php');
    break;

  case 'getLocationEV':
    include('all/getLocationEV.php');
    break;

  case 'resetPassword':
    include('all/resetPassword.php');
    break;



  //rider
  case 'getJob':
    include('rider/getJob.php');
    break;

  case 'startJob':
    include('rider/startJob.php');
    break;

  case 'endJob':
    include('rider/endJob.php');
    break;

  case 'cancelJob':
    include('rider/cancelJob.php');
    break;

  case 'riderLogin':
    include('rider/riderLogin.php');
    break;

  case 'setLocationRider':
    include('rider/setLocationRider.php');
    break;

  case 'setStatusJob':
    include('rider/setStatusJob.php');
    break;

  case 'getRateJob':
    include('rider/getRateJob.php');
    break;

  case 'getDailyRevenue':
    include('rider/getDailyRevenue.php');
    break;

  case 'getListRevenue':
    include('rider/getListRevenue.php');
    break;

  case 'getListJob':
    include('rider/getListJob.php');
    break;

  case 'AEDRider':
      include('rider/manage.php');
      break;

  case 'checkVerAppRider':
    include('rider/checkVersions.php');
    break;

  case 'forgotpwRider':
    include('rider/forgotpassword.php');
    break;

  case 'startJobEV':
    include('rider/startJobEV.php');
    break;

  case 'setLocationEV':
    include('rider/setLocationEV.php');
    break;

  case 'showCallEv':
    include('rider/showCallEv.php');
    break;

  case 'clearEv':
    include('rider/clearEv.php');
    break;

  //user
  case 'setTakeDriver':
    include('user/setTakeDriver.php');
    break;

  case 'getListTakeDriver':
    include('user/getListTakeDriver.php');
    break;

  case 'getLastServiceJob':
    include('user/getLastServiceJob.php');
    break;

  case 'userCancelJob':
    include('user/userCancelJob.php');
    break;


  case 'setSatisfaction':
    include('user/setSatisfaction.php');
    break;

  case 'manageCustomer':
    include('user/manageCustomer.php');
    break;

  case 'customerLogin':
    include('user/customerLogin.php');
    break;

  case 'setCallEv':
    include('user/setCallEv.php');
    break;

   //merchant
    case 'getEVwaypoint':
      include('merchant/getEVwaypoint.php');
      break;

    case 'getMerchantSendMail':
      include('merchant/getMerchantSendMail.php');
      break;

    case 'getMerchantDetail':
      include('merchant/getMerchantDetail.php');
      break;

    case 'getMerchantAll':
      include('merchant/getMerchantAll.php');
      break;

    case 'cart':
      include('merchant/cart.php');
      break;

  default:

    break;
}
exit(json_encode(
  array( "status" => $status , "message" => $message ,
  "data" => $data ,"functionName" => $functionName  ) ));

?>

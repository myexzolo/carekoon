<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    $status = 200;
    $message = 'success';

    $cus_id  = isset($request['cus_id'])?$request['cus_id']:"";

    if($cus_id != "")
    {
      $conDate    = date("Ym");
      // $sql        = "SELECT j.*, c.mobile, r.Mobile as mobile_rider
      //                FROM t_job j
      //                LEFT JOIN t_customer c ON j.cus_id = c.cus_id
      //                LEFT JOIN t_rider r ON j.rider_id = r.rider_id
      //                WHERE j.cus_id = '$cus_id'
      //                ORDER BY j.id_job DESC LIMIT 1";

     $sql        = "SELECT j.*, c.mobile, r.Mobile as mobile_rider, r.*,j.is_active AS j_is_active,
                    (SELECT t.type_car_name FROM t_type_car t WHERE r.Type_car = t.type_car) as type_car_name,
                    (SELECT tt.title_name FROM data_title tt WHERE r.Title = tt.title_no) as title_name
                    FROM t_job j
                    LEFT JOIN t_customer c ON j.cus_id = c.cus_id
                    LEFT JOIN t_rider r ON j.rider_id = r.rider_id
                    WHERE j.cus_id = '$cus_id'
                    ORDER BY j.id_job DESC LIMIT 1";

      $query      = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $count      = $json['dataCount'];
      $row        = $json['data'];

      //echo $sql;

      if($count > 0){
        // start_lat
        // start_lng
        // end_lat
        // end_lng
        // duration
        // distance
        // type_car
        // cus_id
        // rider_id
        // merchant_id
        // price
        // cost
        // receive
        // order_number
        // real_lat
        // real_lng
        // is_active
        // create_date
        $data[0]['start_address']['lat']  = $row[0]['start_lat'];
        $data[0]['start_address']['lng']  = $row[0]['start_lng'];
        $data[0]['start_address']['text']  = $row[0]['start_text'];
        $data[0]['end_address']['lat']    = $row[0]['end_lat'];
        $data[0]['end_address']['lng']    = $row[0]['end_lng'];
        $data[0]['end_address']['text']  = $row[0]['end_text'];
        $data[0]['real_address']['lat']   = $row[0]['real_lat'];
        $data[0]['real_address']['lng']   = $row[0]['real_lng'];
        $data[0]['rate_drive']['price']   = $row[0]['price'];
        $data[0]['rate_drive']['cost']    = $row[0]['cost'];
        $data[0]['rate_drive']['receive'] = $row[0]['receive'];
        $data[0]['rate_drive']['pay_wallet'] = $row[0]['pay_wallet'];
        $data[0]['rate_drive']['pay_cash'] = $row[0]['pay_cash'];
        $data[0]['id_job']   = $row[0]['id_job'];
        $data[0]['duration']   = $row[0]['duration'];
        $data[0]['distance']   = $row[0]['distance'];
        $data[0]['type_car']   = $row[0]['type_car'];
        $data[0]['cus_id']     = $row[0]['cus_id'];
        $data[0]['rider_id']   = $row[0]['rider_id'];
        $data[0]['merchant_id'] = $row[0]['merchant_id'];
        $data[0]['order_number'] = $row[0]['order_number'];
        $data[0]['is_active'] = $row[0]['j_is_active'];
        $data[0]['mobile'] = "+66".substr($row[0]['mobile'],1);
        $data[0]['mobile_rider'] = isset($row[0]['mobile_rider'])?"+66".substr($row[0]['mobile_rider'],1):"";
        $data[0]['rider']['IDCard']    = $row[0]['IDCard'];
        $data[0]['rider']['Email']     = $row[0]['Email'];
        $data[0]['rider']['Mobile']    = isset($row[0]['mobile_rider'])?"+66".substr($row[0]['mobile_rider'],1):"";
        $data[0]['rider']['Title']     = $row[0]['Title'];
        $data[0]['rider']['Title_name']     = $row[0]['title_name'];
        $data[0]['rider']['FirstName']  = $row[0]['FirstName'];
        $data[0]['rider']['LastName']  = $row[0]['LastName'];
        $data[0]['rider']['NickName']  = $row[0]['NickName'];
        $data[0]['rider']['Address']  = $row[0]['Address'];
        $data[0]['rider']['SubDistrictCode']  = $row[0]['SubDistrictCode'];
        $data[0]['rider']['AmphorCode']  = $row[0]['AmphorCode'];
        $data[0]['rider']['ProvinceCode']  = $row[0]['ProvinceCode'];
        $data[0]['rider']['Line']  = $row[0]['Line'];
        $data[0]['rider']['Type_car']  = $row[0]['Type_car'];
        $data[0]['rider']['Type_car_name']  = $row[0]['type_car_name'];
        $data[0]['rider']['Car_no']  = $row[0]['Car_no'];
        $data[0]['rider']['Car_color']  = $row[0]['Car_color'];
        $data[0]['rider']['Car_year']  = $row[0]['Car_year'];
        $data[0]['rider']['Car_brand']  = $row[0]['Car_brand'];
        $data[0]['rider']['Car_model']  = $row[0]['Car_model'];
        $data[0]['rider']['Car_people']  = $row[0]['Car_people'];
        $data[0]['rider']['img_profile']  = "https://www.carekoon.com/image/college/".$row[0]['img_profile'];

        $status = 200;
        $message = 'Success';

      }else{
        $status = 401;
        $message = 'Data Empty';
      }
    }
    else
    {
      $status = 401;
      $message = 'Type Empty';
    }
?>

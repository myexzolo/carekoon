<?php
include('../inc/function/mainFunc.php');
include('../inc/function/connect.php');

$cus_id     = isset($request['cus_id'])?$request['cus_id']:"";
$num_page     = isset($request['num_page'])?$request['num_page']:"";

$pageShow     = 10;
$rowOffset    = ($num_page * $pageShow);

$sql        = "SELECT * FROM t_job
               WHERE cus_id = $cus_id and is_active = 'S'
               ORDER BY end_date DESC
               LIMIT $pageShow OFFSET $rowOffset";

$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$dataCount  = $json['dataCount'];
$row        = $json['data'];

if($dataCount > 0)
{
  $status = 200;
  $message = 'Success';

  for($i=0;$i<$dataCount;$i++)
  {
    $typePay = $row[$i]['pay_cash'];
    $typePayTxt = "คูปอง";
    if($typePay > 0)
    {
      $typePayTxt = "เงินสด";
    }

    $data[$i]['id_job']  = $row[$i]['id_job'];
    $data[$i]['start_text']  = $row[$i]['start_text'];
    $data[$i]['end_text']    = $row[$i]['end_text'];
    $data[$i]['detail']      = DateThai($row[$i]['end_date'])." "."(".$row[$i]['distance']." กม) (".$typePayTxt.")";
    $data[$i]['revenue']     = $row[$i]['receive'];
  }
}
else
{
  $status = 401;
  $message = 'Data Empry';
}








?>

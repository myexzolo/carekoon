<?php
    include('../inc/function/mainFunc.php');
    include('../inc/function/connect.php');

    $status = 200;
    $message = 'success';

    $start_address  = isset($request['start_address'])?$request['start_address']:"";
    $end_address    = isset($request['end_address'])?$request['end_address']:"";
    $duration       = isset($request['duration'])?$request['duration']:"";
    $distance       = isset($request['distance'])?$request['distance']:"";
    $cus_id         = isset($request['cus_id'])?$request['cus_id']:"";
    $type_car       = isset($request['type_car'])?$request['type_car']:"";
    $rate_drive     = isset($request['rate_drive'])?$request['rate_drive']:"";



    $start_lat    = $start_address['lat'];
    $start_lng    = $start_address['lng'];
    $start_text   = $start_address['text'];

    $end_lat    = $end_address['lat'];
    $end_lng    = $end_address['lng'];
    $end_text   = $end_address['text'];
    //$duration
    //$distance
    //$type_car
    //$cus_id
    //$rider_id
    //$merchant_id
    $price      = $rate_drive['price'];
    $cost       = $rate_drive['cost'];
    $receive    = $rate_drive['receive'];
    $pay_wallet = isset($rate_drive['pay_wallet'])?$rate_drive['pay_wallet']:0;
    $pay_cash   = isset($rate_drive['pay_cash'])?$rate_drive['pay_cash']:0;
    $order_number = "";
    //$real_lat
    //$real_lng
    $is_active = "";
    //$create_date

    // SB = รอรับงาน
    // C = ยกเลิก
    // S = สำเร็จ
    // J = รับงาน
    // W = กำลังเดินทาง
    // E = หมดเวลารับงาน

  // start_lat
	// start_lng
	// end_lat
	// end_lng
	// duration
	// distance
	// type_car
	// cus_id
	// rider_id
	// merchant_id
	// price
	// cost
	// receive
	// order_number
	// real_lat
	// real_lng
	// is_active
	// create_date

    if($start_lat != "" && $end_lat != "")
    {
      $order_num = $type_car.date("md");
      $conDate   = date("Ym");

      $is_active = "SB";

      $sql        = "SELECT MAX(order_number) as order_number FROM t_job WHERE order_number LIKE '$order_num%' and  DATE_FORMAT(create_date, '%Y%m') = '$conDate' ";
      $query      = DbQuery($sql,null);
      $json       = json_decode($query, true);
      $orderNumber = $json['data'][0]['order_number'];

      if(isset($orderNumber) && $orderNumber != "")
      {
        $lastNum = substr($orderNumber,strlen($order_num));
        $lastNum = $lastNum + 1;
        $order_number = $order_num.sprintf("%04d", $lastNum);
      }else{
        $order_number = $order_num.sprintf("%04d", 1);
      }

      $sql = "INSERT INTO t_job
                   (start_lat,start_lng,start_text,end_lat,end_lng,end_text,
                    price,cost,receive,order_number,
                    duration,distance,type_car,cus_id,pay_wallet,pay_cash,
                    is_active,create_date)
             VALUES('$start_lat','$start_lng','$start_text','$end_lat','$end_lng','$end_text',
                    '$price','$cost','$receive','$order_number',
                    '$duration','$distance','$type_car','$cus_id','$pay_wallet','$pay_cash',
                    '$is_active',NOW())";

      //echo    $sql;

      $query      = DbQuery($sql,null);
      $row        = json_decode($query, true);
      $errorInfo  = $row['errorInfo'];

      if(intval($row['errorInfo'][0]) == 0){

        $id_job                   = $row['id'];
        $data[0]['id_job']        = $id_job;
        $data[0]['order_number']  = $order_number;

        $serverKey = "AAAA29M7ly0:APA91bEQvumgm58C49tn52aHzeB18WOwV4GKN8RN3nqWk8gdY2qM5ssGmdRMya60_fhtoKmxNftvLBqDKHUKxNcXnhT8sO6zVok0MUd4xVjpBRJWuZAxaODZaeqpeb4iyOgCPdvAN2K0";

        $sql        = "SELECT * FROM t_rider WHERE status_job = 'J' and Type_car = '$type_car' and is_active = 'Y' and token_noti is not null";
        $query      = DbQuery($sql,null);
        $json       = json_decode($query, true);
        $count      = $json['dataCount'];
        $row        = $json['data'];

        for($i=0;$i<$count; $i++)
        {
          $token_noti     = $row[$i]['token_noti'];
          $title          = "รถมาแว้ววววว";
          $body           = "ต้องวิ่งได้แล้วน่ะ !!!!";
          $message_title  = "เฮ้ !! Driver....";
          $text           = "มีคนเรียกนาย";

          $dataArray = array(
            "token" => $token_noti,
            "serverkey" => $serverKey,
            "messageFontApp" => array(
                                'title' =>$title ,
                                'text' => $body,
                                'sound' => 'default',
                                'badge' => '1'
                              ),
            "messageInApp" => array("message_title" => $message_title , "text" => $text, "id_job" => $id_job)
          );

          sendNotify($dataArray);
        }
        //exit();
        $status   = 200;
        $message  = 'Success';


      }else{
        $status = 401;
        $message = 'Fail';
        $data[0]['order_number'] = '';
      }
    }
    else
    {
      $status = 401;
      $message = 'Type Empty';
    }
?>

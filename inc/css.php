<link rel="shortcut icon" type="image/ico" href="../../image/favicon.ico" />

<link href="https://fonts.googleapis.com/css2?family=Kanit&display=swap" rel="stylesheet">
<link href="../../css/font-awesome.min.css" rel="stylesheet">
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/bootstrap-datetimepicker.css" rel="stylesheet"  >
<link href="../../css/datepicker.css" rel="stylesheet">
<link href="../../css/smoke.css" rel="stylesheet">
<link href="../../css/select2.min.css" rel="stylesheet">
<link href="../../css/sweet-alert2.css" rel="stylesheet">
<link href="../../css/main.css" rel="stylesheet">

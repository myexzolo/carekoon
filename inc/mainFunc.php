<?php

function getCurlMapping($arr){
  $json    = json_encode($arr);
  $ch      = curl_init();
  $headers = array(
    'Content-Type: application/json',
    'Authorization: Basic '. base64_encode("MKT02-0002-JP-CONNECT:12345678")
  );
  curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
  curl_setopt($ch, CURLOPT_URL,'https://ws.jpinsurancefriend.com/JPApp/api/GetMapping');
  curl_setopt($ch, CURLOPT_TIMEOUT, 30);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
  $result       = curl_exec ($ch);
  $status_code  = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close ($ch);
  return $result;
}

function calAge($dob){
  return floor((time() - strtotime($dob)) / 31556926);
}

function idCard($personID){
  $rev = strrev($personID);
  $total = 0;
  for($i=1;$i<13;$i++){
   $mul = $i +1;
   $count = $rev[$i]*$mul;
   $total = $total + $count;
  }
  $mod = $total % 11;
  $sub = 11 - $mod;
  $check_digit = $sub % 10;
  return $rev[0] == $check_digit?true:false;
}

function getPath(){
  $rootPath = $_SERVER['DOCUMENT_ROOT'];
  $thisPath = dirname($_SERVER['PHP_SELF']);
  $onlyPath = str_replace($rootPath, '', $thisPath);
  $arr_path = explode("/",$onlyPath);
  return $arr_path[count($arr_path)-1];
}

function uploadfile($attach,$url,$title){
  $fileinfo = pathinfo($attach['name']);
  $filetype = strtolower($fileinfo['extension']);
  $image = $title.time().".jpg";
  move_uploaded_file($attach['tmp_name'],$url.'/'.$image);

  return array('image' => $image);
}

function getAge($birthday) {
  $then = strtotime($birthday);
  return(floor((time()-$then)/31556926));
}

function dateThToEn($date,$format,$delimiter)
{
    $formatLowerCase  = strtolower($format);//var formatLowerCase=format.toLowerCase();
    $formatItems      = explode($delimiter,$formatLowerCase);//var formatItems=formatLowerCase.split(delimiter);
    $dateItems        = explode($delimiter,$date);//var dateItems=date.split(delimiter);
    $monthIndex       = array_search("mm",$formatItems);//var monthIndex=formatItems.indexOf("mm");
    $dayIndex         = array_search("dd",$formatItems);//var dayIndex=formatItems.indexOf("dd");
    $yearIndex        = array_search("yyyy",$formatItems);//var yearIndex=formatItems.indexOf("yyyy");
    $month            = $dateItems[$monthIndex];//var month=parseInt(dateItems[monthIndex]);

    $yearth = $dateItems[$yearIndex];
    if( $yearth > 2450){
      $yearth -= 543;
    }
    $dateEN = $yearth."-".sprintf("%02d", $month)."-".sprintf("%02d", $dateItems[$dayIndex]);
    return $dateEN;
}


?>

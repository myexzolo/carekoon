<?php  $required = "";  ?>

<section>
  <div class="container">
    <div class="alert alert-danger text-center" role="alert">
      กรุณากรอกข้อมูลเป็นภาษาไทยเท่านั้น
    </div>
    <input type="hidden" id="PACKAGE_TYPE">
    <form id="formEmpty" method="post" enctype="multipart/form-data" novalidate>
    <!-- <form id="formEmpty" method="post" action="service/AEDModule.php" enctype="multipart/form-data" novalidate> -->
      <div class="box">
        <div class="row">
          <div class="col-xs-12">
            <div class="header-title">
              <h3>ข้อมูลส่วนตัว</h3>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label>เลขบัตรประชาชน</label>
              <input type="text" name="IDCard" OnKeyPress="return chkNumber(this)" id="idcard" data-smk-pattern="[0-9]{13}" onfocusout="checkIDCard(this.value)" minlength="13" maxlength="13" class="form-control" placeholder="เลขบัตรประชาชน" data-smk-msg="กรอกเลขบัตรประชาชนไม่ถูกต้อง" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label>อีเมล์</label>
              <input type="email" name="Email" class="form-control" data-smk-msg="อีเมล์ไม่ถูกต้อง" placeholder="อีเมล์" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-2">
            <div class="form-group">
              <label>เบอร์โทรศัพท์มือถือ</label>
              <input type="tel" name="Mobile" OnKeyPress="return chkNumber(this)" class="form-control" data-smk-pattern="0[1-9]{1}[0-9]{8}" data-smk-msg="เบอร์โทรศัพท์มือถือไม่ถูกต้อง" placeholder="0xxxxxxxxx" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label>วันเกิด</label>
              <!-- <input type="date" name="Birthday" id="dob" max="<?=date('Y-m-d', strtotime("-55 years"))?>" min="<?=date('Y-m-d', strtotime("-70 years"))?>" class="form-control" placeholder="วันเกิด" > -->
              <input data-smk-msg="&nbsp;" class="form-control datepicker" <?=$required ?> name="Birthday" type="text" data-provide="datepicker" data-date-language="th-th"  style="background-color:#fff;">
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="col-sm-2">
            <div class="form-group">
              <label>คำนำหน้าชื่อ</label>
              <select class="form-control select2" name="Title" placeholder="คำนำหน้าชื่อ" <?=$required ?>>
                <option value="">คำนำหน้าชื่อ</option>
                <?php
                  $sql = "SELECT * FROM data_title";
                  $query = DbQuery($sql,null);
                  $row  = json_decode($query,true);
                  if($row['dataCount'] > 0){
                    foreach ($row['data'] as $value) {
                ?>
                <option value="<?=$value['title_no']?>"><?=$value['title_name']?></option>
                <?php }} ?>
              </select>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label>ชื่อ</label>
              <input type="text" name="FirstName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="ชื่อ" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label>นามสกุล</label>
              <input type="text" name="LastName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="นามสกุล" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-2">
            <div class="form-group">
              <label>ชื่อเล่น</label>
              <input type="text" name="NickName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="ชื่อเล่น" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-12">
            <div class="form-group">
              <label>ที่อยู่</label>
              <input type="text" name="Address" minlength="10" class="form-control" placeholder="ที่อยู่ผู้เอาประกันภัย" <?=$required ?>>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label>รหัสไปรษณีย์</label>
              <input type="text" name="PostCode" id="postcode" minlength="5" maxlength="5" data-smk-msg="กรอกรหัสไปรษณีย์ไม่ถูกต้อง" onfocusout="getAddress('POSTCODE',this.value)" class="form-control" placeholder="รหัสไปรณีย์" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-3 col-xs-6">
            <div class="form-group">
              <label>ตำบล/แขวง</label>
              <select id="subdistrict" name="SubDistrictCode" onfocusout="getDistrict('SUBDISTRICT_CODE',this.value)" class="form-control select2" <?=$required ?>>
                <option value="">ตำบล/แขวง</option>
              </select>
            </div>
          </div>
          <div class="col-sm-3 col-xs-6">
            <div class="form-group">
              <label>อำเภอ/เขต</label>
              <select id="district" name="AmphorCode" class="form-control select2" <?=$required ?>>
                <option value="">อำเภอ/เขต</option>
              </select>
            </div>
          </div>
          <div class="col-sm-3 col-xs-12">
            <div class="form-group">
              <label>จังหวัด</label>
              <select id="province" name="ProvinceCode" class="form-control select2" <?=$required ?>>
                <option value="">จังหวัด</option>
              </select>
            </div>
          </div>

          <div class="col-sm-4 col-xs-6">
            <div class="form-group">
              <label>อาชีพ</label>
              <select class="form-control select2" name="Occp" placeholder="อาชีพ" <?=$required ?>>
                <option value="">อาชีพ</option>
                <?php
                  $sql = "SELECT * FROM t_occ";
                  $query = DbQuery($sql,null);
                  $row  = json_decode($query,true);
                  if($row['dataCount'] > 0){
                    foreach ($row['data'] as $value) {
                ?>
                <option value="<?=$value['occ_id']?>"><?=$value['occ_name']?></option>
                <?php }} ?>
              </select>
            </div>
          </div>
          <div class="col-sm-4 col-xs-6">
            <div class="form-group">
              <label>ID LINE</label>
              <input type="text" name="Line" class="form-control" placeholder="ID LINE" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-4 col-xs-6">
            <div class="form-group">
              <label>วุฒิการศึกษา</label>
              <select class="form-control select2" name="edu_id" placeholder="วุฒิการศึกษา" <?=$required ?>>
                <option value="">วุฒิการศึกษา</option>
                <?php
                  $sql = "SELECT * FROM t_educational";
                  $query = DbQuery($sql,null);
                  $row  = json_decode($query,true);
                  if($row['dataCount'] > 0){
                    foreach ($row['data'] as $value) {
                ?>
                <option value="<?=$value['edu_id']?>"><?=$value['edu_name']?></option>
                <?php }} ?>
              </select>
            </div>
          </div>
        </div>
      </div>

      <!-- เอกสารผู้ขับขี่ -->
      <div class="box">
        <div class="row">
          <div class="col-xs-12">
            <div class="header-title">
              <h3>ข้อมูลการขับขี่รถ</h3>
            </div>
          </div>
          <div class="col-md-12">
            <label>ประเภทใบอนุญาติขับขี่</label>
          </div>
          <div class="col-sm-6">
            <div class="form-group">
              <div class="row">
                <div class="col-sm-6">
                  <div class="radio">
                    <label>
                      <input type="radio" name="type_driver" value="1" <?=$required ?>>
                      ใบขับขี่ส่วนบุคคล
                    </label>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="radio">
                    <label>
                      <input type="radio" name="type_driver" value="2" <?=$required ?>>
                      ใบขับขี่สาธารณะ
                    </label>
                  </div>
                </div>
              </div>


            </div>
          </div>
          <!-- <div class="col-sm-3">
            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="radio" name="type_driver" value="2" <?=$required ?>>
                  ใบขับขี่ส่วนบุคคล
                </label>
              </div>
            </div>
          </div> -->

          <div class="clearfix"></div>

          <div class="col-sm-3">
            <div class="form-group">
              <input type="text" name="driver_license" class="form-control" data-smk-msg="เลขที่ใบขับขี่" placeholder="เลขที่ใบขับขี่">
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="col-sm-3">
            <div class="form-group">
              <label>ประเภทรถ</label>
              <select class="form-control select2" name="Type_car" placeholder="ประเภทรถ" <?=$required ?>>
                <option value="">ประเภทรถ</option>
                <?php
                  $sql = "SELECT * FROM t_type_car";
                  $query = DbQuery($sql,null);
                  $row  = json_decode($query,true);
                  if($row['dataCount'] > 0){
                    foreach ($row['data'] as $value) {
                ?>
                <option value="<?=$value['type_car']?>"><?=$value['type_car_name']?></option>
                <?php }} ?>
              </select>
            </div>
          </div>

          <div class="col-sm-3">
            <div class="form-group">
              <label>ป้ายทะเบียน</label>
              <input type="text" name="Car_no" class="form-control" data-smk-msg="ป้ายทะเบียน" placeholder="ป้ายทะเบียน" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label>สีรถ</label>
              <input type="text" name="Car_color" class="form-control" data-smk-msg="สีรถ" placeholder="สีรถ" <?=$required ?>>
            </div>
          </div>

          <div class="clearfix"></div>

          <div class="col-sm-3">
            <div class="form-group">
              <label>ปีรถ</label>
              <input type="text" name="Car_year" class="form-control" data-smk-msg="ปีรถยนต์" placeholder="ปีรถยนต์" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label>ยี่ห้อรถ</label>
              <input type="text" name="Car_brand" class="form-control" data-smk-msg="ยี่ห้อรถยนต์" placeholder="ยี่ห้อรถยนต์" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label>รุ่นรถ</label>
              <input type="text" name="Car_model" class="form-control" data-smk-msg="รุ่นรถยนต์" placeholder="รุ่นรถยนต์" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-3">
            <div class="form-group">
              <label>จำนวนผู้โดยสาร</label>
              <input type="number" name="Car_people" class="form-control" data-smk-msg="จำนวนผู้โดยสาร" placeholder="จำนวนผู้โดยสาร" <?=$required ?>>
            </div>
          </div>
        </div>
      </div>

      <!-- ผู้เกี่ยวข้อง -->
      <div class="box">
        <div class="row">
          <div class="col-xs-12">
            <div class="header-title">
              <h3>ข้อมูลติดต่อกลับกรณีฉุกเฉิน</h3>
            </div>
          </div>

          <div class="col-sm-4">
            <div class="form-group">
              <label>ชื่อ</label>
              <input type="text" name="Emergency_firstName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="ชื่อ" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-4">
            <div class="form-group">
              <label>นามสกุล</label>
              <input type="text" name="Emergency_lastName" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="นามสกุล" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-2">
            <div class="form-group">
              <label>ความสัมพันธ์</label>
              <input type="text" name="Emergency_relation" class="form-control" data-smk-pattern="^[ก-๏\s]+$" data-smk-msg="ภาษาไทยเท่านั้น" placeholder="ความสัมพันธ์" <?=$required ?>>
            </div>
          </div>
          <div class="col-sm-2">
            <div class="form-group">
              <label>เบอร์โทรศัพท์มือถือ</label>
              <input type="tel" name="Emergency_Mobile" class="form-control" data-smk-pattern="0[1-9]{1}[0-9]{8}" data-smk-msg="เบอร์โทรศัพท์มือถือไม่ถูกต้อง" placeholder="0xxxxxxxxx" <?=$required ?>>
            </div>
          </div>

        </div>
      </div>

      <!-- เอกสารแนบ -->
      <div class="box">
        <div class="row">
          <div class="col-xs-12">
            <div class="header-title">
              <h3>เอกสารแนบ</h3>
            </div>
          </div>
          <div class="col-xs-12 under-line">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label>รูปถ่าย 1 นิ้ว</label>
                  <input type="file" name="img_profile" <?=$required ?> onchange="readURL(this,'showImgIdcard1')" accept="image/*">
                  <p class="help-block">Upload ไฟล์ .jpg, .jpeg, .png เท่านั้น</p>
                </div>
              </div>
              <div class="col-sm-6">
                <!-- <p>ตัวอย่างไฟล์</p> -->
                <div id="showImgIdcard1">
                  <!-- <img src="../../image/IDCARD.png" width="150"> -->
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 under-line">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label>สำเนาบัตรประชาชน</label>
                  <input type="file" name="img_idcard" <?=$required ?> onchange="readURL(this,'showImgIdcard2')" accept="image/*">
                  <p class="help-block">Upload ไฟล์ .jpg, .jpeg, .png เท่านั้น</p>
                </div>
              </div>
              <div class="col-sm-6">
                <!-- <p>ตัวอย่างไฟล์</p> -->
                <div id="showImgIdcard2">
                  <!-- <img src="../../image/IDCARD.png" width="150"> -->
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 under-line">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label>สำเนาทะเบียนบ้าน</label>
                  <input type="file" name="img_home" <?=$required ?> onchange="readURL(this,'showImgIdcard3')" accept="image/*">
                  <p class="help-block">Upload ไฟล์ .jpg, .jpeg, .png เท่านั้น</p>
                </div>
              </div>
              <div class="col-sm-6">
                <!-- <p>ตัวอย่างไฟล์</p> -->
                <div id="showImgIdcard3">
                  <!-- <img src="../../image/IDCARD.png" width="150"> -->
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 under-line">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label>สำเนาใบอนุญาตขับขี่</label>
                  <input type="file" name="img_driver_id" <?=$required ?> onchange="readURL(this,'showImgIdcard4')" accept="image/*">
                  <p class="help-block">Upload ไฟล์ .jpg, .jpeg, .png เท่านั้น</p>
                </div>
              </div>
              <div class="col-sm-6">
                <!-- <p>ตัวอย่างไฟล์</p> -->
                <div id="showImgIdcard4">
                  <!-- <img src="../../image/IDCARD.png" width="150"> -->
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 under-line">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label>สำเนาใบจดทะเบียนรถ/กรณีไม่ใช่เจ้าของรถให้เจ้าของรถเขียนใบยินยอมแทน</label>
                  <input type="file" name="img_copy_car" <?=$required ?> onchange="readURL(this,'showImgIdcard5')" accept="image/*">
                  <p class="help-block">Upload ไฟล์ .jpg, .jpeg, .png เท่านั้น</p>
                </div>
              </div>
              <div class="col-sm-6">
                <!-- <p>ตัวอย่างไฟล์</p> -->
                <div id="showImgIdcard5">
                  <!-- <img src="../../image/IDCARD.png" width="150"> -->
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 under-line">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label>สำเนา พรบ./รายการเสียภาษี</label>
                  <input type="file" name="img_act" <?=$required ?> onchange="readURL(this,'showImgIdcard6')" accept="image/*">
                  <p class="help-block">Upload ไฟล์ .jpg, .jpeg, .png เท่านั้น</p>
                </div>
              </div>
              <div class="col-sm-6">
                <!-- <p>ตัวอย่างไฟล์</p> -->
                <div id="showImgIdcard6">
                  <!-- <img src="../../image/IDCARD.png" width="150"> -->
                </div>
              </div>
            </div>
          </div>
          <div class="col-xs-12 under-line">
            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <label>สำเนาสมุดบัญชีธนาคาร (รูปหน้าแรก)</label>
                  <input type="file" name="img_bookbank" <?=$required ?> onchange="readURL(this,'showImgIdcard7')" accept="image/*">
                  <p class="help-block">Upload ไฟล์ .jpg, .jpeg, .png เท่านั้น</p>
                </div>
              </div>
              <div class="col-sm-6">
                <!-- <p>ตัวอย่างไฟล์</p> -->
                <div id="showImgIdcard7">
                  <!-- <img src="../../image/IDCARD.png" width="150"> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- เงื่อนไขการรับประกันภัย -->
      <div class="box">
        <div class="row">
          <div class="col-xs-12">
            <div class="header-title">
              <h3>เงื่อนไขและข้อตกลงในการใช้งาน</h3>
            </div>
          </div>
          <div class="col-xs-12">

          </div>
          <div class="col-xs-12">
            <div class="text-consent">
              <?php
                  $sql = "SELECT term FROM t_policy where name = 'rider'";
                  $query = DbQuery($sql,null);
                  $json  = json_decode($query,true);
                  $row   = $json['data'];

                  echo $row[0]['term'];
              ?>
            </div>
            <div class="form-group">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="check" <?=$required ?>>
                  ยอมรับเงื่อนไข
                </label>
              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg btn-submit">ลงทะเบียน</button>
          </div>
        </div>
      </div>
    </form>
  </div>
</section>

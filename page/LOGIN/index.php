<?php include "../../inc/mainFunc.php"; ?>
<?php include "../../inc/connect.php"; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Carekoon Driver - Carekoon</title>

    <?php include "../../inc/css.php"; ?>
    <link href="css/home.css" rel="stylesheet">
  </head>
  <body>
    <?php include "../../inc/nav.php"; ?>
    <section>
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="header-title">
              <h1>สมัครสมาชิก Carekoon Driver</h1>
              <ol class="breadcrumb">
                <li><a href="javaScript:void(0)">ประจำวัน</a></li>
                <li class="active"><?=date('d/m/Y');?></li>
              </ol>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include "ajax/information.php"; ?>


    <?php include "../../inc/js.php"; ?>
    <script type="text/javascript" src="https://t.2c2p.com/securepayment/api/my2c2p.1.6.9.min.js "></script>
    <script src="js/home.js"></script>

    <script type="text/javascript">
      // getPackage();
    </script>
  </body>
</html>

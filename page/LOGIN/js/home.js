
$("#formEmpty").on('submit', function(event) {
  event.preventDefault();
  /* Act on the event */
    if ($('#formEmpty').smkValidate()) {
      Swal.fire({
        title: 'ต้องการดำเนินการต่อ?',
        text: "ตรวจสอบข้อมูลให้ถูกต้อง!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน!',
        cancelButtonText: 'ยกเลิก!'
        }).then((result) => {
        if (result.value) {
          $.ajax({
              url: 'service/AEDModule.php',
              type: 'POST',
              data: new FormData( this ),
              processData: false,
              contentType: false,
              dataType: 'json'
          }).done(function( data ) {
            $.smkAlert({text: data.message,type: data.status});
          });
        }
      });
    }

});


function chkNumber(ele)
{
    var vchar = String.fromCharCode(event.keyCode);
    if ((vchar<'0' || vchar>'9') && (vchar != '.')) return false;
    ele.onKeyPress=vchar;
}

function getAddress(name,value){
  $.post( "service/getAddress.php", { name: name, value: value })
  .done(function( data ) {
    var district = $('#subdistrict').html('<option value="">ตำบล/แขวง</option>');
    if(data.status == 200){
      var selected = '';
      $('#subdistrict').html('');
      $.each(data.data , function(i, field){
        selected = (i == 0) ? 'selected' :'';
        $('#subdistrict').append('<option value="'+field.SUBDISTRICT_CODE+'" '+selected+'>'+field.SUBDISTRICT_NAME+'</option>');
      });
      getDistrict('SUBDISTRICT_CODE',data.data[0].SUBDISTRICT_CODE);
      $('#province').html('');
      $('#province').append('<option value="'+data.data[0].PROVINCE_CODE+'" '+selected+'>'+data.data[0].PROVINCE_NAME+'</option>');
      var subdistrict = $('#subdistrict').val();
    }
  });
}

function getDistrict(name,value){
  $.post( "service/getAddress.php", { name: name, value: value })
  .done(function( data ) {
    var district = $('#district').html('<option value="">อำเภอ/เขต</option>');
    if(data.status == 200){
      var selected = '';
      $('#district').html('');
      $.each(data.data , function(i, field){
        selected = (i == 0) ? 'selected' :'';
        $('#district').append('<option value="'+field.DISTRICT_CODE+'" '+selected+'>'+field.DISTRICT_NAME+'</option>');
      });
    }
  });
}

function checkIDCard(idCard){

  if(idCard.length == 13){
    var idcard = $("#idcard");
    $.post( "service/checkIdcard.php", { idCard: idCard })
    .done(function( data ) {
      console.log(data);
      if(data.status == 401){
        Swal.fire({
          type: 'error',
          title: 'เกิดข้อผิดพลาด',
          text: data.message,
        });
        idcard.val('');
        //idcard.val(idcard.val().substring(-1, idcard.val().length-1));
        //$("#idcard").focus();
      }
    });
  }
}

$('.select2').select2();

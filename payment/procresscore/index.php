<?php include "../../inc/mainFunc.php"; ?>
<?php include "../../inc/connect.php"; ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>123 SERVICE</title>

    <?php include "../../inc/css.php"; ?>
    <link href="css/home.css" rel="stylesheet">
  </head>
  <body>


    <?php
      $merchantID = "764764000002901";
    	$secretKey = "FB4B8CBFCAE457A6B5A96F146248B5F319E96AC38DD9739EA50D8AEBFB79D340";


      $sql   = "SELECT * FROM [JP_NONMOTOR].[dbo].[SET_PACKAGE_OFFLINE] WHERE PACKAGE_ID = '{$_POST['PackageID']}'";
      $query = DbQuery($sql,null);
      $row   = json_decode($query,true)['data'][0];


      $TIME_LIMIT = $row['TIME_LIMIT'];
      $desc = $row['DETAIL'];
      $amt  = str_pad($row['PRICE']*100,12,"0",STR_PAD_LEFT);
      $_POST['GrossPrem'] = $row['PRICE'];
      $uniqueTransactionCode = "99".time().rand(0,9);
      $cardholderName = $_POST['FirstName'].' '.$_POST['LastName'];
      $cardholderEmail = $_POST['Email'];
      $mobileNo = $_POST['Mobile'];
      $_POST['sms'] = '66'.substr($_POST['Mobile'],1);
      $version = "9.9";
      $panCountry = "TH";
      $currencyCode = "764";

      //Encrypted card data
      $xml = '';
      $paymentChannel = "123";
      $agentCode = "SCB";	// SCB
      $channelCode = "IBANKING";	// IBANKING
      $paymentExpiry = date("Y-m-d H:i:s", strtotime("+1 day",strtotime(date('Y-m-d H:i:s'))));
      $xml = "<PaymentRequest>
        <merchantID>$merchantID</merchantID>
        <uniqueTransactionCode>$uniqueTransactionCode</uniqueTransactionCode>
        <desc>$desc</desc>
        <amt>$amt</amt>
        <currencyCode>$currencyCode</currencyCode>
        <panCountry></panCountry>
        <cardholderName></cardholderName>
        <paymentChannel>$paymentChannel</paymentChannel>
        <agentCode>$agentCode</agentCode>
        <channelCode>$channelCode</channelCode>
        <paymentExpiry>$paymentExpiry</paymentExpiry>
        <mobileNo>$mobileNo</mobileNo>
        <cardholderEmail>$cardholderEmail</cardholderEmail>
        <encCardData></encCardData>
        </PaymentRequest>";

      $arr = array();
      $arr['TABLE_NAME'] = base64_decode($_POST['TABLE_NAME']);
      $arr['uniqueTransactionCode'] = $uniqueTransactionCode;

      unset($_POST['TABLE_NAME']);
      unset($_POST['check']);
      unset($_POST['encryptedCardInfo']);
      unset($_POST['maskedCardInfo']);
      unset($_POST['expMonthCardInfo']);
      unset($_POST['expYearCardInfo']);

      $_POST['Age'] = getAge($_POST['Birthday']);
      $_POST['uniqueTransactionCode'] = $uniqueTransactionCode;
      $_POST['IMG_IDCARD'] = uploadfile($_FILES['img_idcard'],'../../image/IDCARD',$_POST['uniqueTransactionCode'])['image'];


      $sqld = "SELECT
              getdate() AS Edate,
              dateadd(month,($TIME_LIMIT),getdate()) AS Xdate
              FROM [JPCONNECT].[dbo].[OFFLINE_NONMOTOR]";
      $queryd = DbQuery($sqld,null);
      $jsond  = json_decode($queryd,true)['data'][0];

      $_POST['Edate'] = $jsond['Edate'];
      $_POST['Xdate'] = $jsond['Xdate'];



      $sql = '';
      $sql .= DBInsertPOST($_POST,$arr['TABLE_NAME']);
      $sql .= DBInsertPOST($arr,'[JP_MASTER].[dbo].[DB_TABLE_PAY]');
      DbQuery($sql,null);
      // echo $sql;

      $xml = $xml;
    	$paymentPayload = base64_encode($xml);
    	$signature = strtoupper(hash_hmac('sha256', $paymentPayload, $secretKey, false));
    	$payloadXML = "<PaymentRequest>
               <version>$version</version>
               <payload>$paymentPayload</payload>
               <signature>$signature</signature>
               </PaymentRequest>";
    	$payload = base64_encode($payloadXML);

    ?>

    <p class="text-center">Processing payment request, Do not close the browser, press back or refresh the page.</p>


    <form action='https://t.2c2p.com/SecurePayment/PaymentAuth.aspx' method='POST' name='paymentRequestForm'>
    	<?php echo "<input type='hidden' name='paymentRequest' value='".$payload."'>"; ?>
    </form>

    <?php include "../../inc/js.php"; ?>
    <script src="js/home.js"></script>

    <script language="JavaScript">
      setTimeout(function(){
        document.paymentRequestForm.submit();
      }, 3000);
    </script>


  </body>
</html>
